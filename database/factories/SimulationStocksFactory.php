<?php

namespace Database\Factories;

use App\Models\SimulationStocks;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class SimulationStocksFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SimulationStocks::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $status = Arr::random([platform('status_code.simulation_stocks.buy'), platform('status_code.simulation_stocks.sell')]);
        $buyPrice = mt_rand(10, 100);

        return [
            'user_id' => 1,
            'status' => $status,
            'buy_stocks' => 1,
            'buy_price' => $buyPrice,
            'buy_date' => now()->subDays(1)->toDateString()
        ];
    }
}
