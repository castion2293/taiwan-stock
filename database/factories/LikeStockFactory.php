<?php

namespace Database\Factories;

use App\Models\LikeStock;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class LikeStockFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LikeStock::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition()
    {
        $status = Arr::random([platform('status_code.like_stocks.add_like'), platform('status_code.like_stocks.remove_like')]);

        return [
            'user_id' => 1,
            'status' => $status,
            'like_at' => now()->subDays(1)->toDateString()
        ];
    }
}
