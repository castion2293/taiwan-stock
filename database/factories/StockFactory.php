<?php

namespace Database\Factories;

use App\Models\Stock;
use Illuminate\Database\Eloquent\Factories\Factory;

class StockFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Stock::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $lastEndPrice = $startPrice = floatval(mt_rand(100, 10000) / 100);
        $endPrice = $startPrice + ($startPrice * (rand(-10, 10) / 100));

        return [
            'stocks' => $this->faker->numberBetween(100, 10000),
            'start_price' => $startPrice,
            'end_price' => $endPrice,
            'last_end_price' => $lastEndPrice,
            'price_diff' => $endPrice - $lastEndPrice,
            'price_diff_percent' => (($endPrice - $lastEndPrice) / $lastEndPrice) * 100
        ];
    }
}
