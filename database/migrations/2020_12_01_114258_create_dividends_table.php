<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDividendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dividends', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('PK');
            $table->string('stock_no', 10)->comment('股票代碼');
            $table->string('stock_name', 10)->comment('股票名稱');
            $table->date('divided_date')->comment('除權息日');
            $table->decimal('dividing_price', 6, 2)->default(0)->comment('除權息前股價');
            $table->decimal('dividend', 6, 2)->default(0)->comment('現金股利 (元/股)');
            $table->decimal('dividend_percent', 6, 2)->default(0)->comment('現金殖利率');
            $table->decimal('stock_dividend', 6, 2)->default(0)->comment('股票股利 (元/百股)');
            $table->decimal('free_dividend', 6, 2)->default(0)->comment('無償配股率');
            $table->decimal('issuance_stock_dividend', 6, 2)->default(0)->comment('現增配股率');
            $table->decimal('call_price', 6, 2)->default(0)->comment('認購價 (元/股)');
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('建立時間');
            $table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->comment('最後更新');

            // 索引
            $table->unique(['stock_no', 'divided_date']);
        });

        DB::statement("ALTER TABLE `" . "dividends" . "` COMMENT '股利表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dividends');
    }
}
