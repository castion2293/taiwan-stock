<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSimulationStocksSoldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simulation_stocks_sold', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('PK');
            $table->unsignedInteger('user_id')->comment('會員id');
            $table->string('stock_no', 10)->comment('股票代碼');
            $table->string('stock_name', 10)->comment('股票名稱');
            $table->unsignedInteger('sale_stocks')->comment('販賣張數');
            $table->decimal('buy_price', 6, 2)->comment('購買成交均價');
            $table->decimal('sale_price', 6, 2)->default(0)->comment('販賣成交價格');
            $table->decimal('profit_loss', 6, 2)->default(0)->comment('損益');
            $table->decimal('profit_loss_percent', 6, 2)->default(0)->comment('報酬率(%)');
            $table->date('sale_date')->comment('販賣日期');
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('建立時間');
            $table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->comment('最後更新');

            // 索引
            $table->index(['user_id', 'stock_no']);
        });

        DB::statement("ALTER TABLE `" . "simulation_stocks_sold" . "` COMMENT '模擬股票賣出'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simulation_stocks_sold');
    }
}
