<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGoldCrossAndTurnUpToSimulationStocksBoughtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('simulation_stocks_bought', function (Blueprint $table) {
            $table->boolean('turn_up')->default(false)->after('turn_down')->comment('均線反轉上揚');
            $table->boolean('gold_cross')->default(false)->after('turn_down')->comment('均線黃金交叉');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('simulation_stocks_bought', function (Blueprint $table) {
            $table->dropColumn('turn_up');
            $table->dropColumn('gold_cross');
        });
    }
}
