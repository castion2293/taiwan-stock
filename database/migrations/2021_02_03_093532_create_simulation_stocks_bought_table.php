<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSimulationStocksBoughtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simulation_stocks_bought', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('PK');
            $table->unsignedInteger('user_id')->comment('會員id');
            $table->string('stock_no', 10)->comment('股票代碼');
            $table->string('stock_name', 10)->comment('股票名稱');
            $table->unsignedInteger('buy_stocks')->comment('購買張數');
            $table->unsignedInteger('sale_stocks')->default(0)->comment('販賣張數');
            $table->decimal('buy_price', 6, 2)->comment('購買成交均價');
            $table->date('buy_date')->comment('購買日期');
            $table->boolean('all_sold')->default(false)->comment('全部賣出');
            $table->boolean('death_cross')->default(false)->comment('均線死亡交叉');
            $table->boolean('turn_down')->default(false)->comment('均線反轉下彎');
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('建立時間');
            $table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->comment('最後更新');

            // 索引
            $table->index(['user_id', 'stock_no', 'all_sold']);
        });

        DB::statement("ALTER TABLE `" . "simulation_stocks_bought" . "` COMMENT '模擬股票買入'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simulation_stocks_bought');
    }
}
