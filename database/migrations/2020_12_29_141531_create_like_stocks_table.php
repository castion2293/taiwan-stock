<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_stocks', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('PK');
            $table->unsignedInteger('user_id')->comment('會員id');
            $table->string('stock_no', 10)->comment('股票代碼');
            $table->string('stock_name', 10)->comment('股票名稱');
            $table->unsignedTinyInteger('status')->default(1)->comment('狀態 (1: 加入, 2: 移除)');
            $table->date('like_at')->comment('加入欲買股票日期');
            $table->date('remove_at')->default('9999-12-31')->comment('移除日期');
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('建立時間');
            $table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->comment('最後更新');

            // 索引
            $table->index(['user_id', 'stock_no', 'status']);
        });

        DB::statement("ALTER TABLE `" . "like_stocks" . "` COMMENT '欲買股票'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_stocks');
    }
}
