<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttentionStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attention_stocks', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('PK');
            $table->unsignedInteger('user_id')->comment('會員id');
            $table->string('stock_no', 10)->comment('股票代碼');
            $table->string('stock_name', 10)->comment('股票名稱');
            $table->unsignedTinyInteger('status')->default(1)->comment('狀態 (1: 關注中, 2: 已達標, 3: 停止關注)');
            $table->decimal('price', 6, 2)->comment('開始關注價格');
            $table->decimal('target_price', 6, 2)->comment('目標價');
            $table->boolean('greater')->comment('1: 大於目標價, 0: 小於目標價');
            $table->datetime('attended_at')->default('9999-12-31 00:00:00')->comment('達標時間點');
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('建立時間');
            $table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->comment('最後更新');

            // 索引
            $table->index(['user_id', 'stock_no', 'status']);
        });

        DB::statement("ALTER TABLE `" . "attention_stocks" . "` COMMENT '關注股票'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attention_stocks');
    }
}
