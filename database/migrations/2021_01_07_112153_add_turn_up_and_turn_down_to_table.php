<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTurnUpAndTurnDownToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rise_up_stocks', function (Blueprint $table) {
            $table->boolean('turn_up')->default(false)->after('gold_cross')->comment('均線反轉上揚');
        });

        Schema::table('like_stocks', function (Blueprint $table) {
            $table->boolean('turn_up')->default(false)->after('remove_at')->comment('均線反轉上揚');
            $table->boolean('gold_cross')->default(false)->after('remove_at')->comment('均線黃金交叉');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rise_up_stocks', function (Blueprint $table) {
            $table->dropColumn('turn_up');
        });

        Schema::table('like_stocks', function (Blueprint $table) {
            $table->dropColumn('turn_up');
            $table->dropColumn('gold_cross');
        });
    }
}
