<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('stock_no', 10)->comment('股票代碼');
            $table->string('stock_name', 10)->comment('股票名稱');
            $table->date('date')->comment('日期');
            $table->unsignedInteger('stocks')->comment('成交張數');
            $table->decimal('start_price', 6, 2)->comment('開盤價');
            $table->decimal('end_price', 6, 2)->comment('收盤價');
            $table->decimal('last_end_price', 6, 2)->comment('昨天收盤價');
            $table->decimal('price_diff', 6, 2)->comment('漲跌價差');
            $table->decimal('price_diff_percent', 6, 2)->comment('漲跌價差百分比(%)');
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('建立時間');
            $table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->comment('最後更新');

            // 索引
            $table->unique(['stock_no', 'date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
