<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompareDailyStockStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('like_stocks', function (Blueprint $table) {
            $table->boolean('compare_daily_stock')->default(false)->after('turn_up')->comment('成交倍增');
        });

        Schema::table('simulation_stocks_bought', function (Blueprint $table) {
            $table->boolean('compare_daily_stock')->default(false)->after('turn_up')->comment('成交倍增');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('like_stocks', function (Blueprint $table) {
            $table->dropColumn('compare_daily_stock');
        });

        Schema::table('simulation_stocks_bought', function (Blueprint $table) {
            $table->dropColumn('compare_daily_stock');
        });
    }
}
