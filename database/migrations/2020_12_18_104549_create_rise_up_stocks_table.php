<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiseUpStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rise_up_stocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('stock_no', 10)->comment('股票代碼');
            $table->string('stock_name', 10)->comment('股票名稱');
            $table->date('start_date')->comment('開始計算交易日');
            $table->date('end_date')->comment('結束計算交易日期');
            $table->integer('recent_rise_up_days')->default(0)->comment('最近連續上漲天數');
            $table->integer('longest_rise_up_days')->default(0)->comment('最長連續上漲天數');
            $table->integer('monthly_rise_up_days')->default(0)->comment('一個月內上漲總天數');
            $table->integer('recent_red_k_days')->default(0)->comment('最近連續紅Ｋ天數');
            $table->integer('longest_red_k_days')->default(0)->comment('最長連續紅Ｋ天數');
            $table->integer('monthly_red_k_days')->default(0)->comment('一個月內紅Ｋ總天數');
            $table->boolean('gold_cross')->default(false)->comment('均線黃金交叉');
            $table->datetime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('建立時間');
            $table->datetime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->comment('最後更新');

            // 索引
            $table->unique(['stock_no', 'end_date']);
            $table->index('end_date');
        });

        DB::statement("ALTER TABLE `" . "rise_up_stocks" . "` COMMENT '連續上漲股票'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rise_up_stocks');
    }
}
