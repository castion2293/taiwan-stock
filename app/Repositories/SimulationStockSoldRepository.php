<?php

namespace App\Repositories;

use App\Models\SimulationStocksSold;
use App\Scopes\SimulationStockSoldScope;
use Illuminate\Support\Facades\DB;

class SimulationStockSoldRepository extends AbstractBaseRepository
{
    public function __construct(SimulationStocksSold $simulationStocksSold)
    {
        $this->model = $simulationStocksSold;
        $this->table = $this->model->getTable();
    }

    /**
     * 已實損益 status = 2
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getSimulationStockList(array $params)
    {
        // model scope 過濾條件方式
        $stockBuilder = $this->scopeQuery(SimulationStockSoldScope::class, $params);

        return $stockBuilder->select(
            'id',
            'stock_no',
            'stock_name',
            'sale_stocks',
            'buy_price',
            'sale_price',
            'sale_date',
            DB::raw("(profit_loss * 1000) as profit_loss"),
            'profit_loss_percent'
        )
            ->where('user_id', auth()->user()->id)
            ->paginate(25)
            ->toArray();
    }
}
