<?php

namespace App\Repositories;

use App\Models\Stock;
use App\Scopes\StockScope;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StockRepository extends AbstractBaseRepository
{
    public function __construct(Stock $stock)
    {
        $this->model = $stock;
        $this->table = $this->model->getTable();
    }

    /**
     * 獲取股價最新價格
     *
     * @param string $stockNo
     * @param string $date
     * @return float
     */
    public function getLastEndPrice(string $stockNo, string $date)
    {
        $endPrice = $this->model::select('end_price')
            ->where('stock_no', $stockNo)
            ->whereBetween('date', [Carbon::parse($date)->subWeeks(1)->toDateString(), $date])
            ->orderBy('date', 'desc')
            ->first();

        return floatval(data_get($endPrice, 'end_price'));
    }

    /**
     * 獲取最近兩個交易日股票資訊
     *
     * @param array $stockNos
     * @param string $date
     * @return array
     */
    public function getLastTwoWeekDayStockInfo(array $stockNos, string $date)
    {
        $lastWeekDate = Carbon::parse($date)->previousWeekday()->toDateString();

        $compareStocks = Stock::select(
            'id',
            'stock_no',
            'stock_name',
            'date',
            'stocks',
            'start_price',
            'end_price',
            'last_end_price',
            'price_diff',
            'price_diff_percent'
        )
            ->whereIn('stock_no', $stockNos)
            ->whereBetween('date', [$lastWeekDate, $date])
            ->orderBy('id', 'desc')
            ->get()
            ->groupBy('stock_no')
            ->toArray();

        return $compareStocks;
    }

    /**
     * 獲取一個月內的股票資訊 做計算連續上漲使用
     *
     * @param array $stockNos
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function getRiseUpStocksForOneMonth(array $stockNos, string $startDate, string $endDate)
    {
        return Stock::select(
            'stock_no',
            'stock_name',
            'date',
            'price_diff',
            DB::raw('(end_price - start_price) as today_price_diff')
        )
            ->whereIn('stock_no', $stockNos)
            ->whereBetween('date', [$startDate, $endDate])
            ->orderBy('id', 'desc')
            ->get()
            ->groupBy('stock_no')
            ->toArray();
    }

    /**
     * 今日行情股票資訊
     *
     * @param array $params
     * @return mixed
     */
    public function getStockLists(array $params)
    {
        // model scope 過濾條件方式
        $stockBuilder = $this->scopeQuery(StockScope::class, $params);

        return $stockBuilder->select(
            'id',
            'stock_no',
            'stock_name',
            'date',
            'stocks',
            'end_price',
            'last_end_price',
            'price_diff',
            'price_diff_percent',
            DB::raw('TIME(updated_at) AS renew_at')
        )
            ->paginate(25)
            ->toArray();
    }

    /**
     * 獲取某日的均線黃金交叉股票資訊
     *
     * @param string $date
     * @param array $stockNos
     * @return mixed
     */
    public function getStocksByDateAndStockNo(string $date, array $stockNos)
    {
        return $this->model->select('stock_no', 'stock_name', 'date', 'stocks', 'end_price')
            ->whereIn('stock_no', $stockNos)
            ->where('date', $date)
            ->where('stocks', '>', 1000)
            ->get()
            ->toArray();
    }
}
