<?php

namespace App\Repositories;

use App\Models\FiveMinutesStocks;
use App\Models\Stock;
use App\Scopes\FiveMinutesStockScope;
use App\Scopes\StockScope;
use Illuminate\Support\Facades\DB;

class FiveMinutesStocksRepository extends AbstractBaseRepository
{
    public function __construct(FiveMinutesStocks $fiveMinutesStocks)
    {
        $this->model = $fiveMinutesStocks;
        $this->table = $this->model->getTable();
    }

    /**
     * 瞬間巨量股票資訊
     *
     * @param array $params
     * @return mixed
     */
    public function getFiveMinutesStockLists(array $params)
    {
        $stockSubQuery = $this->scopeQuery(StockScope::class, ['current' => true], \App::make(Stock::class))
            ->select('*');

        // model scope 過濾條件方式
        $stockBuilder = $this->scopeQuery(FiveMinutesStockScope::class, $params);

        return $stockBuilder->select(
            "{$this->table}.id",
            "{$this->table}.stock_no",
            "{$this->table}.stock_name",
            "{$this->table}.date",
            "{$this->table}.new_five_minutes_stocks",
            "{$this->table}.stocks",
            "{$this->table}.end_price as five_minutes_price",
            "{$this->table}.price_diff",
            "{$this->table}.price_diff_percent",
            DB::raw('TIME(' . $this->table . '.updated_at) AS renew_at'),
            "last_stocks.end_price"
        )
            ->joinSub(
                $stockSubQuery,
                'last_stocks',
                function ($join) {
                    $join->on("{$this->table}.stock_no", '=', 'last_stocks.stock_no');
                }
            )
            ->paginate(25)
            ->toArray();
    }
}
