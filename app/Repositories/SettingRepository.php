<?php

namespace App\Repositories;

use App\Models\Setting;
use Illuminate\Support\Arr;

class SettingRepository extends AbstractBaseRepository
{
    public function __construct(Setting $setting)
    {
        $this->model = $setting;
        $this->table = $this->model->getTable();
    }

    /**
     * 獲取所有的環境設定
     *
     * @return array
     */
    public function getAllSettingKeyValue()
    {
        return collect($this->getAll(['key', 'value']))->mapWithKeys(function ($setting) {
            return [
                  Arr::get($setting, 'key') => Arr::get($setting, 'value')
            ];
        })->toArray();
    }
}
