<?php

namespace App\Repositories;

use App\Models\AttentionStock;
use App\Models\Stock;
use App\Scopes\AttentionStockScope;
use App\Scopes\StockScope;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class AttentionStockRepository extends AbstractBaseRepository
{
    public function __construct(AttentionStock $attentionStock)
    {
        $this->model = $attentionStock;
        $this->table = $this->model->getTable();
    }

    /**
     * 觀注股票資訊
     *
     * @param array $params
     * @return array
     */
    public function getAttentionStockList(array $params)
    {
        $status = Arr::get($params, 'status');

        $stockSubQuery = $this->scopeQuery(StockScope::class, ['current' => true], \App::make(Stock::class))
            ->select('*');

        // model scope 過濾條件方式
        $stockBuilder = $this->scopeQuery(AttentionStockScope::class, $params);

        return $stockBuilder->select(
            "{$this->table}.id",
            "{$this->table}.stock_no",
            "{$this->table}.stock_name",
            "{$this->table}.status",
            "{$this->table}.price",
            "{$this->table}.target_price",
            "{$this->table}.greater",
            "{$this->table}.attended_at",
            "last_stocks.end_price",
        )
            ->joinSub(
                $stockSubQuery,
                'last_stocks',
                function ($join) {
                    $join->on("{$this->table}.stock_no", '=', 'last_stocks.stock_no');
                }
            )
            ->where('user_id', auth()->user()->id)
            ->where('status', $status)
            ->paginate(25)
            ->toArray();
    }

    /**
     * 獲取要檢查的關注股票 cursor 物件
     *
     * @param string $date
     * @return \Illuminate\Support\LazyCollection
     */
    public function getAttentionStockCursor(string $date)
    {
        $stockQuery = DB::table('stocks')
            ->where('date', $date);

        return DB::table($this->table)
            ->select(
                "{$this->table}.id",
                "{$this->table}.stock_no",
                "{$this->table}.stock_name",
                "{$this->table}.target_price",
                "{$this->table}.greater",
                'latest_stocks.end_price'
            )
            ->where('status', 1)
            ->joinSub(
                $stockQuery,
                'latest_stocks',
                function ($join) {
                    $join->on("{$this->table}.stock_no", '=', 'latest_stocks.stock_no');
                }
            )->cursor();
    }
}
