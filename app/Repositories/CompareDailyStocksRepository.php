<?php

namespace App\Repositories;

use App\Models\CompareDailyStocks;
use App\Scopes\CompareDailyStockScope;
use Illuminate\Support\Facades\DB;

class CompareDailyStocksRepository extends AbstractBaseRepository
{
    public function __construct(CompareDailyStocks $compareDailyStocks)
    {
        $this->model = $compareDailyStocks;
        $this->table = $this->model->getTable();
    }

    /**
     * 成交倍增股市資訊
     *
     * @param array $params
     * @return mixed
     */
    public function getCompareDailyStockLists(array $params)
    {
        // model scope 過濾條件方式
        $stockBuilder = $this->scopeQuery(CompareDailyStockScope::class, $params);

        return $stockBuilder->select(
            'id',
            'stock_no',
            'stock_name',
            'date',
            'stocks',
            'last_stocks',
            DB::raw('(stocks / last_stocks) AS stocks_increase'),
            'end_price',
            'price_diff',
            'price_diff_percent',
            DB::raw('TIME(updated_at) AS renew_at')
        )
            ->paginate(25)
            ->toArray();
    }
}
