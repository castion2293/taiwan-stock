<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends AbstractBaseRepository
{
    public function __construct(User $user)
    {
        $this->model = $user;
        $this->table = $this->model->getTable();
    }

    /**
     * 扣除會員額度
     *
     * @param int $id
     * @param int $amount
     * @return mixed
     */
    public function deductBalance(int $id, int $amount)
    {
        return $this->model
            ->lockForUpdate()
            ->find($id)
            ->decrement('balance', $amount);
    }

    /**
     * 增加會員額度
     *
     * @param int $id
     * @param int $amount
     * @return mixed
     */
    public function addBalance(int $id, int $amount)
    {
        return $this->model
            ->lockForUpdate()
            ->find($id)
            ->increment('balance', $amount);
    }
}
