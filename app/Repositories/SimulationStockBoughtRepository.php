<?php

namespace App\Repositories;

use App\Models\SimulationStocksBought;
use App\Models\Stock;
use App\Scopes\SimulationStockBoughtScope;
use App\Scopes\StockScope;
use Illuminate\Support\Facades\DB;

class SimulationStockBoughtRepository extends AbstractBaseRepository
{
    public function __construct(SimulationStocksBought $simulationStocksBought)
    {
        $this->model = $simulationStocksBought;
        $this->table = $this->model->getTable();
    }

    /**
     * 庫存損益 status = 1
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getSimulationStockList(array $params)
    {
        $stockSubQuery = $this->scopeQuery(StockScope::class, ['current' => true], \App::make(Stock::class))
            ->select('*');

        // model scope 過濾條件方式
        $stockBuilder = $this->scopeQuery(SimulationStockBoughtScope::class, $params);

        return $stockBuilder->select(
            "{$this->table}.id",
            "{$this->table}.stock_no",
            "{$this->table}.stock_name",
            DB::raw("({$this->table}.buy_stocks - {$this->table}.sale_stocks) as buy_stocks"),
            "{$this->table}.buy_price",
            DB::raw("(({$this->table}.buy_stocks - {$this->table}.sale_stocks) * {$this->table}.buy_price) * 1000 as cost"),
            "{$this->table}.buy_date",
            "{$this->table}.death_cross",
            "{$this->table}.turn_down",
            "{$this->table}.gold_cross",
            "{$this->table}.turn_up",
            "{$this->table}.compare_daily_stock",
            "last_stocks.end_price",
            "last_stocks.price_diff as today_price_diff",
            DB::raw("((last_stocks.end_price - {$this->table}.buy_price) * ({$this->table}.buy_stocks - {$this->table}.sale_stocks)) * 1000 as price_diff"),
            DB::raw(
                "((last_stocks.end_price - {$this->table}.buy_price) / {$this->table}.buy_price) * 100 as price_diff_percent"
            ),
        )
            ->joinSub(
                $stockSubQuery,
                'last_stocks',
                function ($join) {
                    $join->on("{$this->table}.stock_no", '=', 'last_stocks.stock_no');
                }
            )
            ->where('user_id', auth()->user()->id)
            ->where("{$this->table}.all_sold", '=', false)
            ->paginate(25)
            ->toArray();
    }

    /**
     * 帶入指定股票代號 確認有買入的股票
     *
     * @param array $stockNos
     * @return mixed
     * @throws \Exception
     */
    public function getBoughtStockNosByMultiStockNos(array $stockNos)
    {
        return $this->model->select('stock_no')
            ->where('user_id', auth()->user()->id)
            ->whereIn('stock_no', $stockNos)
            ->where('all_sold', false)
            ->get()
            ->pluck('stock_no')
            ->toArray();
    }


    /**
     * 更新均線死亡交叉狀態
     *
     * @param array $stockNos
     * @return mixed
     * @throws \Exception
     */
    public function updateDeathCrossCrossStatus(array $stockNos)
    {
        return $this->model->whereIn('stock_no', $stockNos)
            ->where('all_sold', false)
            ->update(['death_cross' => true]);
    }

    /**
     * 更新均線反轉下彎狀態
     *
     * @param array $stockNos
     * @return mixed
     * @throws \Exception
     */
    public function updateTurnDownStatus(array $stockNos)
    {
        return $this->model->whereIn('stock_no', $stockNos)
            ->where('all_sold', false)
            ->update(['turn_down' => true]);
    }

    /**
     * 更新均線黃金交叉狀態
     *
     * @param array $stockNos
     * @return mixed
     */
    public function updateGoldCrossStatus(array $stockNos)
    {
        return $this->model->whereIn('stock_no', $stockNos)
            ->where('all_sold', false)
            ->update(['gold_cross' => true]);
    }

    /**
     * 更新均線反轉上揚狀態
     *
     * @param array $stockNos
     * @return mixed
     */
    public function updateTurnUpStatus(array $stockNos)
    {
        return $this->model->whereIn('stock_no', $stockNos)
            ->where('all_sold', false)
            ->update(['turn_up' => true]);
    }

    /**
     * 更新成交倍增狀態
     *
     * @param string $stockNo
     * @return mixed
     */
    public function updateCompareDailyStockStatus(string $stockNo)
    {
        return $this->model->where('stock_no', $stockNo)
            ->where('all_sold', false)
            ->update(['compare_daily_stock' => true]);
    }
}
