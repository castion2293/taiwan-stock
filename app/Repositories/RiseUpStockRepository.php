<?php

namespace App\Repositories;

use App\Models\RiseUpStock;
use App\Models\Stock;
use App\Scopes\RiseUpStockScope;
use App\Scopes\StockScope;

class RiseUpStockRepository extends AbstractBaseRepository
{
    public function __construct(RiseUpStock $stock)
    {
        $this->model = $stock;
        $this->table = $this->model->getTable();
    }

    /**
     * 連續上漲股票資訊
     *
     * @param array $params
     * @return mixed
     */
    public function getRiseUpStockLists(array $params)
    {
        $stockSubQuery = $this->scopeQuery(StockScope::class, ['current' => true], \App::make(Stock::class))
            ->select('*');

        // model scope 過濾條件方式
        $stockBuilder = $this->scopeQuery(RiseUpStockScope::class, $params);

        return $stockBuilder->select(
            "{$this->table}.id",
            "{$this->table}.stock_no",
            "{$this->table}.stock_name",
            "{$this->table}.end_date",
            "{$this->table}.recent_rise_up_days",
            "{$this->table}.longest_rise_up_days",
            "{$this->table}.monthly_rise_up_days",
            "{$this->table}.recent_red_k_days",
            "{$this->table}.longest_red_k_days",
            "{$this->table}.monthly_red_k_days",
            "{$this->table}.gold_cross",
            "{$this->table}.turn_up",
            "last_stocks.end_price",
            "last_stocks.stocks"
        )
            ->joinSub(
                $stockSubQuery,
                'last_stocks',
                function ($join) {
                    $join->on("{$this->table}.stock_no", '=', 'last_stocks.stock_no');
                }
            )
            ->where('last_stocks.stocks', '>=', 1000)
            ->paginate(25)
            ->toArray();
    }

    /**
     * 更新均線黃金交叉狀態
     *
     * @param string $endDate
     * @param array $stockNos
     * @return mixed
     */
    public function updateGoldCrossStatus(string $endDate, array $stockNos)
    {
        return $this->model->whereIn('stock_no', $stockNos)
            ->where('end_date', $endDate)
            ->update(['gold_cross' => true]);
    }

    /**
     * 更新均線反轉上揚狀態
     *
     * @param string $endDate
     * @param array $stockNos
     * @return mixed
     */
    public function updateTurnUpStatus(string $endDate, array $stockNos)
    {
        return $this->model->whereIn('stock_no', $stockNos)
            ->where('end_date', $endDate)
            ->update(['turn_up' => true]);
    }
}
