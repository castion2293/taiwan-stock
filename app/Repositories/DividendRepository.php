<?php

namespace App\Repositories;

use App\Models\Dividend;
use App\Scopes\DividendScope;
use Illuminate\Support\Facades\DB;

class DividendRepository extends AbstractBaseRepository
{
    public function __construct(Dividend $dividend)
    {
        $this->model = $dividend;
        $this->table = $this->model->getTable();
    }

    /**
     * 股票股利資訊
     *
     * @param array $params
     * @return array
     */
    public function getDividendList(array $params): array
    {
        // model scope 過濾條件方式
        $stockBuilder = $this->scopeQuery(DividendScope::class, $params);

        return $stockBuilder->select(
            "stock_no",
            "stock_name",
            DB::raw("YEAR(`divided_date`) as year"),
            DB::raw("SUM(`dividend`) as dividend"),
            DB::raw("COUNT(`id`) as times"),
            DB::raw("SUM(`dividing_price`) / COUNT(`id`) as dividing_price"),
            DB::raw("(SUM(`dividend`) / (SUM(`dividing_price`) / COUNT(`id`))) * 100 as dividend_percent")
        )
            ->groupBy('stock_no', 'year')
            ->paginate(25)
            ->toArray();
    }
}
