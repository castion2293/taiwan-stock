<?php

namespace App\Repositories;

use App\Models\LikeStock;
use App\Models\Stock;
use App\Scopes\LikeStockScope;
use App\Scopes\StockScope;

class LikeStockRepository extends AbstractBaseRepository
{
    public function __construct(LikeStock $likeStock)
    {
        $this->model = $likeStock;
        $this->table = $this->model->getTable();
    }

    /**
     * 欲買股票 status = 1
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getLikeStockListForAdd(array $params): array
    {
        $stockSubQuery = $this->scopeQuery(StockScope::class, ['current' => true], \App::make(Stock::class))
            ->select('*');

        // model scope 過濾條件方式
        $stockBuilder = $this->scopeQuery(LikeStockScope::class, $params);

        return $stockBuilder->select(
            "{$this->table}.id",
            "{$this->table}.stock_no",
            "{$this->table}.stock_name",
            "{$this->table}.like_at",
            "{$this->table}.gold_cross",
            "{$this->table}.turn_up",
            "{$this->table}.compare_daily_stock",
            "last_stocks.end_price",
            "last_stocks.price_diff",
            "last_stocks.price_diff_percent",
        )
            ->joinSub(
                $stockSubQuery,
                'last_stocks',
                function ($join) {
                    $join->on("{$this->table}.stock_no", '=', 'last_stocks.stock_no');
                }
            )
            ->where('user_id', auth()->user()->id)
            ->where("{$this->table}.status", platform('status_code.like_stocks.add_like'))
            ->paginate(25)
            ->toArray();
    }

    /**
     * 帶入指定股票代號 確認加入欲買清單的股票
     *
     * @param array $stockNos
     * @return mixed
     * @throws \Exception
     */
    public function getLikeStocksByMultiStocksNos(array $stockNos)
    {
        return $this->model->select('stock_no')
            ->where('user_id', auth()->user()->id)
            ->whereIn('stock_no', $stockNos)
            ->where('status', platform('status_code.like_stocks.add_like'))
            ->get()
            ->pluck('stock_no')
            ->toArray();
    }

    /**
     * 更新均線黃金交叉狀態
     *
     * @param array $stockNos
     * @return mixed
     * @throws \Exception
     */
    public function updateGoldCrossStatus(array $stockNos)
    {
        return $this->model->whereIn('stock_no', $stockNos)
            ->where('status', platform('status_code.like_stocks.add_like'))
            ->update(['gold_cross' => true]);
    }

    /**
     * 更新均線反轉上揚狀態
     *
     * @param array $stockNos
     * @return mixed
     * @throws \Exception
     */
    public function updateTurnUpStatus(array $stockNos)
    {
        return $this->model->whereIn('stock_no', $stockNos)
            ->where('status', platform('status_code.like_stocks.add_like'))
            ->update(['turn_up' => true]);
    }

    /**
     * 更新成交倍增狀態
     *
     * @param string $stocksNo
     * @return mixed
     * @throws \Exception
     */
    public function updateCompareDailyStockStatus(string $stocksNo)
    {
        return $this->model->where('stock_no', $stocksNo)
            ->where('status', platform('status_code.like_stocks.add_like'))
            ->update(['compare_daily_stock' => true]);
    }
}
