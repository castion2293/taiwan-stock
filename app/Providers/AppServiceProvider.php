<?php

namespace App\Providers;

use App\Models\Setting;
use App\Observers\SettingObserver;
use App\Repositories\SettingRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local')) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Setting::observe(SettingObserver::class);

        // -----------------
        //   初始化時 setting config
        // -----------------
        $this->initialSettingsConfig();

        // macro
        Str::macro(
            'wrap',
            static function ($value, $wrap) {
                return Str::of($value)->start($wrap)->finish($wrap);
            }
        );
    }

    /**
     * 初始化時 setting config
     */
    private function initialSettingsConfig()
    {
        // 如果已經有快取 從快取取得
        if (Cache::tags(['settings'])->has('config')) {
            $settings = Cache::tags(['settings'])->get('config');

            foreach ($settings as $key => $setting) {
                $diff = array_diff_key(config("setting.{$key}"), $setting);

                $setting = array_merge($setting, $diff);
                Config::set($key, $setting);
            }
            return ;
        }

        // 從資料庫取得
        if (Schema::hasTable('settings')) {
            $settingRepository = App::make(SettingRepository::class);
            $settings = $settingRepository->getAllSettingKeyValue();

            foreach ($settings as $key => $setting) {
                $diff = array_diff_key(config("setting.{$key}"), $setting);

                $setting = array_merge($setting, $diff);
                Config::set($key, $setting);
            }

            Cache::tags(['settings'])->put('config', $settings);
        }
    }
}
