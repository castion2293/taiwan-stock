<?php

namespace App\Services;

use App\Channels\SlackWebhookChannel;
use App\Crawlers\DeathCrossCrawler;
use App\Crawlers\DirectionTurnCrawler;
use App\Crawlers\GoldenCrossCrawler;
use App\Notifications\GoldenCrossStockNotification;
use App\Notifications\TurnUpStockNotification;
use App\Repositories\LikeStockRepository;
use App\Repositories\RiseUpStockRepository;
use App\Repositories\SimulationStockBoughtRepository;
use App\Repositories\StockRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class CrossStockService
{
    /**
     * @var GoldenCrossCrawler
     */
    protected $goldenCrossCrawler;

    /**
     * @var DeathCrossCrawler
     */
    protected $deathCrossCrawler;

    /**
     * @var RiseUpStockRepository
     */
    protected $riseUpStockRepository;

    /**
     * @var StockRepository
     */
    protected $stockRepository;

    /**
     * @var DirectionTurnCrawler
     */
    protected $directionTurnCrawler;

    /**
     * @var LikeStockRepository
     */
    protected $likeStockRepository;

    /**
     * @var SimulationStockBoughtRepository
     */
    protected $simulationStockBoughtRepository;

    public function __construct(
        GoldenCrossCrawler $goldenCrossCrawler,
        DeathCrossCrawler $deathCrossCrawler,
        DirectionTurnCrawler $directionTurnCrawler,
        RiseUpStockRepository $riseUpStockRepository,
        StockRepository $stockRepository,
        SimulationStockBoughtRepository $simulationStockBoughtRepository,
        LikeStockRepository $likeStockRepository
    ) {
        $this->goldenCrossCrawler = $goldenCrossCrawler;
        $this->deathCrossCrawler = $deathCrossCrawler;
        $this->riseUpStockRepository = $riseUpStockRepository;
        $this->stockRepository = $stockRepository;
        $this->directionTurnCrawler = $directionTurnCrawler;
        $this->likeStockRepository = $likeStockRepository;
        $this->simulationStockBoughtRepository = $simulationStockBoughtRepository;
    }

    /**
     * 抓取居均線黃金交叉股票
     *
     * @return array
     * @throws \Exception
     */
    public function fetchGoldenCross(): array
    {
        try {
            $allGoldenCrossStocks = $this->goldenCrossCrawler->crawler();

            $stockCodes = platform('stock_code');
            $date = Arr::get($allGoldenCrossStocks, 'date');
            $stockNos = array_intersect(Arr::get($allGoldenCrossStocks, 'stock_no'), array_keys($stockCodes));

            DB::transaction(
                function () use ($date, $stockNos) {
                    $this->simulationStockBoughtRepository->updateGoldCrossStatus($stockNos);

                    $this->riseUpStockRepository->updateGoldCrossStatus(
                        $date,
                        $stockNos
                    );

                    $this->likeStockRepository->updateGoldCrossStatus($stockNos);
                }
            );

            $goldenCrossStocks = $this->stockRepository->getStocksByDateAndStockNo($date, $stockNos);
            foreach ($goldenCrossStocks as $goldenCrossStock) {
                // 發告警
                Notification::channel(SlackWebhookChannel::class)->sendMessage(
                    new GoldenCrossStockNotification($goldenCrossStock)
                );
            }

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 抓取居均線死亡交叉股票
     *
     * @return array
     * @throws \Exception
     */
    public function fetchDeathCross(): array
    {
        try {
            $allDeathCrossStocks = $this->deathCrossCrawler->crawler();

            $stockCodes = platform('stock_code');
            $stockNos = array_intersect(Arr::get($allDeathCrossStocks, 'stock_no'), array_keys($stockCodes));

            DB::transaction(
                function () use ($stockNos) {
                    $this->simulationStockBoughtRepository->updateDeathCrossCrossStatus($stockNos);
                }
            );

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 抓取均線方向改變股票
     *
     * @return array
     * @throws \Exception
     */
    public function fetchDirectionTurn(): array
    {
        try {
            $allDirectionTurnStocks = $this->directionTurnCrawler->crawler();

            DB::transaction(
                function () use ($allDirectionTurnStocks) {
                    $stockCodes = platform('stock_code');
                    $date = Arr::get($allDirectionTurnStocks, 'date');
                    $turnUpStockNos = array_intersect(
                        Arr::get($allDirectionTurnStocks, 'turn_up_stock_no'),
                        array_keys($stockCodes)
                    );
                    $turnDownStockNos = array_intersect(
                        Arr::get($allDirectionTurnStocks, 'turn_down_stock_no'),
                        array_keys($stockCodes)
                    );

                    // 均線反轉上揚
                    $this->riseUpStockRepository->updateTurnUpStatus($date, $turnUpStockNos);

                    $this->likeStockRepository->updateTurnUpStatus($turnUpStockNos);

                    $this->simulationStockBoughtRepository->updateTurnUpStatus($turnUpStockNos);

                    $turnUpStocks = $this->stockRepository->getStocksByDateAndStockNo($date, $turnUpStockNos);

                    $turnUpStocksChunk = collect($turnUpStocks)->chunk(10)->toArray();
                    foreach ($turnUpStocksChunk as $chunk) {
                        foreach ($chunk as $turnUpStock) {
                            // 發告警
                            Notification::channel(SlackWebhookChannel::class)->sendMessage(
                                new TurnUpStockNotification($turnUpStock)
                            );
                        }
                        sleep(5);
                    }

                    // 均線反轉下彎
                    $this->simulationStockBoughtRepository->updateTurnDownStatus($turnDownStockNos);
                }
            );

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }
}
