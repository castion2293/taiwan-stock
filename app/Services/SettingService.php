<?php


namespace App\Services;

use App\Repositories\SettingRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class SettingService
{
    /**
     * @var SettingRepository
     */
    protected $settingRepository;

    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    /**
     * 環境設定列表
     */
    public function getList(): array
    {
        try {
            $dataBaseSettings = $this->settingRepository->getAllSettingKeyValue();
            $configSettings = config('setting');

            $settings = collect($configSettings)->map(
                function ($configSetting, $settingKey) use ($dataBaseSettings) {
                    if (!isset($dataBaseSettings[$settingKey])) {
                        return $configSetting;
                    }

                    // 如果DB已經有設定值 以DB的數值為主
                    foreach ($configSetting as $valueKey => $value) {
                        if (isset($dataBaseSettings[$settingKey][$valueKey])) {
                            $configSetting[$valueKey] = $dataBaseSettings[$settingKey][$valueKey];
                        }
                    }

                    return $configSetting;
                }
            )->toArray();

            return [
                'code' => platform('api_code.success'),
                'data' => [
                    'viewData' => $settings
                ]
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 更新設定
     *
     * @param string $key
     * @param array $value
     * @return array
     * @throws \Exception
     */
    public function updateSetting(string $key, array $value)
    {
        try {
            DB::transaction(
                function () use ($key, $value) {
                    $oldSetting = $this->settingRepository->findByWhere(['key' => $key], 'value');
                    $raw = Arr::first(Arr::pluck($oldSetting, 'value')) ?? [];

                    $this->settingRepository->updateOrInsert(
                        [
                            'key' => $key
                        ],
                        [
                            'key' => $key,
                            'value' => json_encode($value),
                            'raw' => json_encode($raw)
                        ]
                    );
                }
            );

            // 清除 setting 快取
            Cache::tags(['settings'])->flush();

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }
}
