<?php

namespace App\Services;

use App\Repositories\LikeStockRepository;
use App\Repositories\SimulationStockBoughtRepository;
use App\Repositories\SimulationStockRepository;
use App\Repositories\SimulationStockSoldRepository;
use App\Repositories\StockRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class SimulationStockService
{
    protected $repositoryNameArray = [];

    /**
     * @var StockRepository
     */
    protected $stockRepository;

    /**
     * @var LikeStockRepository
     */
    protected $likeStockRepository;

    /**
     * @var SimulationStockBoughtRepository
     */
    protected $simulationStockBoughtRepository;

    /**
     * @var SimulationStockSoldRepository
     */
    protected $simulationStockSoldRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(
        StockRepository $stockRepository,
        LikeStockRepository $likeStockRepository,
        SimulationStockBoughtRepository $simulationStockBoughtRepository,
        SimulationStockSoldRepository $simulationStockSoldRepository,
        UserRepository $userRepository
    ) {
        $this->stockRepository = $stockRepository;
        $this->likeStockRepository = $likeStockRepository;
        $this->simulationStockBoughtRepository = $simulationStockBoughtRepository;
        $this->simulationStockSoldRepository = $simulationStockSoldRepository;
        $this->userRepository = $userRepository;

        $this->repositoryNameArray = [
            platform('status_code.simulation_stocks.buy') => 'simulationStockBoughtRepository',
            platform('status_code.simulation_stocks.sell') => 'simulationStockSoldRepository'
        ];
    }

    /**
     * 模擬買賣股票資訊
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getList(array $params)
    {
        try {
            $status = intval(Arr::get($params, 'status', platform('status_code.simulation_stocks.buy')));

            $repositoryName = Arr::get($this->repositoryNameArray, $status);

            if (empty($repositoryName)) {
                throw new \Exception("Repository不存在");
            }

            $simulationStocks = $this->$repositoryName->getSimulationStockList($params);

            // 如果是庫存損益列表 status = 1 需加上 action = sell_simulation_stock
            if ($status === platform('status_code.simulation_stocks.buy')) {
                $stocksData = Arr::get($simulationStocks, 'data');

                $simulationStocks['data'] = collect($stocksData)->map(
                    function ($stock) {
                        return array_merge(
                            $stock,
                            [
                                'action' => platform('status_code.action.sell_stock')
                            ]
                        );
                    }
                )->toArray();
            }

            return [
                'code' => platform('api_code.success'),
                'data' => [
                    'viewData' => $simulationStocks
                ]
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 模擬買入股票
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function buySimulationStock(array $params)
    {
        try {
            DB::transaction(
                function () use ($params) {
                    // 扣除會員額度
                    $this->userRepository->deductBalance(auth()->id(), Arr::get($params, 'amount'));

                    $stockNo = Arr::get($params, 'stock_no');

                    $simulationStocksBoughts = $this->simulationStockBoughtRepository->findByWhere(
                        [
                            'user_id' => Arr::get($params, 'user_id'),
                            'stock_no' => $stockNo,
                            'all_sold' => false
                        ]
                    );

                    $simulationStocksBought = Arr::first($simulationStocksBoughts);

                    if (!empty($simulationStocksBought)) {
                        // 已經存在模擬股票買入中 更新數量及成交均價
                        $id = Arr::get($simulationStocksBought, 'id');
                        $buyStocks = Arr::get($simulationStocksBought, 'buy_stocks');
                        $saleStocks = Arr::get($simulationStocksBought, 'sale_stocks');
                        $buyPrice = Arr::get($simulationStocksBought, 'buy_price');
                        $addStocks = Arr::get($params, 'buy_stocks');
                        $buyDate = now()->toDateString();
                        $currentPrice = $this->stockRepository->getLastEndPrice($stockNo, $buyDate);

                        $simulationStocksBoughtUpdateParams = [
                            'buy_stocks' => $buyStocks + $addStocks,
                            'buy_price' => (($buyStocks - $saleStocks) * $buyPrice + $addStocks * $currentPrice) / (($buyStocks - $saleStocks) + $addStocks),
                            'buy_date' => $buyDate
                        ];

                        $this->simulationStockBoughtRepository->update($id, $simulationStocksBoughtUpdateParams);
                    } else {
                        // 建立新的模擬股票買入
                        $simulationStocksBoughtStoreParams = [
                            'user_id' => Arr::get($params, 'user_id'),
                            'stock_no' => $stockNo,
                            'stock_name' => platform("stock_code.{$stockNo}"),
                            'buy_stocks' => Arr::get($params, 'buy_stocks'),
                            'buy_date' => $buyDate = now()->toDateString(),
                            'buy_price' => $this->stockRepository->getLastEndPrice($stockNo, $buyDate),
                        ];

                        $this->simulationStockBoughtRepository->store($simulationStocksBoughtStoreParams);
                    }

                    // 如果有在欲買清單內 就把它從欲買清單中移除
                    $likeStock = $this->likeStockRepository->findByWhere(
                        [
                            'user_id' => Arr::get($params, 'user_id'),
                            'stock_no' => $stockNo,
                            'status' => platform('status_code.like_stocks.add_like'),
                        ],
                        'id'
                    );

                    if (empty($likeStock)) {
                        return;
                    }

                    $likeStockId = Arr::first(Arr::pluck($likeStock, 'id'));

                    $this->likeStockRepository->update(
                        $likeStockId,
                        [
                            'status' => platform('status_code.like_stocks.remove_like'),
                        ]
                    );
                }
            );
            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 模擬賣出股票
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function sellSimulationStock(array $params)
    {
        try {
            DB::transaction(
                function () use ($params) {
                    $simulationStockBoughtId = Arr::get($params, 'simulation_stock_bought_id');

                    // 更新模擬買入庫存股票資料
                    $simulationStockBought = $this->simulationStockBoughtRepository->find($simulationStockBoughtId);

                    $id = Arr::get($simulationStockBought, 'id');
                    $buyStocks = Arr::get($simulationStockBought, 'buy_stocks');
                    $saleStocks = Arr::get($simulationStockBought, 'sale_stocks');
                    $addSaleStocks = Arr::get($params, 'sale_stocks');

                    $simulationStocksBoughtUpdateParams = [
                        'sale_stocks' => $saleStocks + $addSaleStocks,
                        'all_sold' => ($saleStocks + $addSaleStocks) >= $buyStocks
                    ];

                    $this->simulationStockBoughtRepository->update($id, $simulationStocksBoughtUpdateParams);

                    // 新增模擬賣出股票資料
                    $simulationStockSoldStoreParams = [
                        'user_id' => $userId = Arr::get($simulationStockBought, 'user_id'),
                        'stock_no' => $stockNo = Arr::get($simulationStockBought, 'stock_no'),
                        'stock_name' => Arr::get($simulationStockBought, 'stock_name'),
                        'sale_stocks' => $addSaleStocks,
                        'buy_price' => $buyPrice = Arr::get($simulationStockBought, 'buy_price'),
                        'sale_price' => $salePrice = $this->stockRepository->getLastEndPrice(
                            $stockNo,
                            now()->toDateString()
                        ),
                        'profit_loss' => ($profitLoss = $salePrice - $buyPrice) * $addSaleStocks,
                        'profit_loss_percent' => ($profitLoss / $buyPrice) * 100,
                        'sale_date' => now()->toDateString()
                    ];

                    $this->simulationStockSoldRepository->store($simulationStockSoldStoreParams);

                    // 增加會員額度
                    $this->userRepository->addBalance($userId, $salePrice * 1000 * $addSaleStocks);
                }
            );
            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 重置模擬買入股票均線狀態
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function resetSimulationStock(array $params)
    {
        try {
            DB::transaction(
                function () use ($params) {
                    $simulationStockBoughtId = Arr::get($params, 'id');
                    $this->simulationStockBoughtRepository->update(
                        $simulationStockBoughtId,
                        [
                            'death_cross' => false,
                            'turn_down' => false,
                            'gold_cross' => false,
                            'turn_up' => false,
                            'compare_daily_stock' => false
                        ]
                    );
                }
            );

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 檢查股票是否已經模擬買過
     *
     * @param array $stockNos
     * @return array
     * @throws \Exception
     */
    public function checkSimulationStocks(array $stockNos): array
    {
        try {
            $boughtStockNos = $this->simulationStockBoughtRepository->getBoughtStockNosByMultiStockNos($stockNos);

            $checkSimulationStocks = collect($stockNos)->mapWithKeys(
                function ($stockNo) use ($boughtStockNos) {
                    return [
                        $stockNo => in_array($stockNo, $boughtStockNos)
                    ];
                }
            )->toArray();

            return [
                'code' => platform('api_code.success'),
                'result' => $checkSimulationStocks,
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }
}
