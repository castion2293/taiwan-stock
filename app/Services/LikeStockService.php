<?php

namespace App\Services;

use App\Repositories\LikeStockRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class LikeStockService
{
    /**
     * @var LikeStockRepository
     */
    protected $likeStockRepository;

    public function __construct(LikeStockRepository $likeStockRepository)
    {
        $this->likeStockRepository = $likeStockRepository;
    }

    /**
     * 欲買股票清單
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getList(array $params): array
    {
        try {
            $likeStocks = $this->likeStockRepository->getLikeStockListForAdd($params);

            return [
                'code' => platform('api_code.success'),
                'data' => [
                    'viewData' => $likeStocks
                ]
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 新增欲買股票
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function addLikeStock(array $params): array
    {
        try {
            DB::transaction(
                function () use ($params) {
                    $likeStockStoreParams = [
                        'user_id' => Arr::get($params, 'user_id'),
                        'stock_no' => $stockNo = Arr::get($params, 'stock_no'),
                        'stock_name' => platform("stock_code.{$stockNo}"),
                        'status' => platform('status_code.like_stocks.add_like'),
                        'like_at' => now()->toDateString()
                    ];

                    return $this->likeStockRepository->store($likeStockStoreParams);
                }
            );

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 移除欲買股票
     *
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function removeLikeStock(int $id): array
    {
        try {
            DB::transaction(
                function () use ($id) {
                    $this->likeStockRepository->update(
                        $id,
                        [
                            'status' => platform('status_code.like_stocks.remove_like'),
                        ]
                    );
                }
            );

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 重置欲買股票均線狀態
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function resetLikeStock(array $params)
    {
        try {
            DB::transaction(
                function () use ($params) {
                    $likeStockId = Arr::get($params, 'id');
                    $this->likeStockRepository->update(
                        $likeStockId,
                        [
                            'gold_cross' => false,
                            'turn_up' => false,
                            'compare_daily_stock' => false
                        ]
                    );
                }
            );

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 檢查股票是否加入欲買股票
     *
     * @param array $stockNos
     * @return array
     * @throws \Exception
     */
    public function checkLikeStocks(array $stockNos): array
    {
        try {
            $likeStockNos = $this->likeStockRepository->getLikeStocksByMultiStocksNos($stockNos);

            $checkLikeStocks = collect($stockNos)->mapWithKeys(
                function ($stockNo) use ($likeStockNos) {
                    return [
                        $stockNo => in_array($stockNo, $likeStockNos)
                    ];
                }
            )->toArray();

            return [
                'code' => platform('api_code.success'),
                'result' => $checkLikeStocks,
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }
}
