<?php

namespace App\Services;

use App\Channels\SlackWebhookChannel;
use App\Notifications\CompareStockNotification;
use App\Repositories\CompareDailyStocksRepository;
use App\Repositories\LikeStockRepository;
use App\Repositories\SimulationStockBoughtRepository;
use App\Repositories\StockRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Notification;

class CompareDailyStockService
{
    use CheckStockTrait;
    /**
     * @var StockRepository
     */
    protected $stockRepository;

    /**
     * @var CompareDailyStocksRepository
     */
    protected $compareDailyStocksRepository;

    /**
     * @var LikeStockRepository
     */
    protected $likeStockRepository;

    /**
     * @var SimulationStockBoughtRepository
     */
    protected $simulationStockBoughtRepository;

    public function __construct(
        StockRepository $stockRepository,
        CompareDailyStocksRepository $compareDailyStocksRepository,
        LikeStockRepository $likeStockRepository,
        SimulationStockBoughtRepository $simulationStockBoughtRepository
    ) {
        $this->stockRepository = $stockRepository;
        $this->compareDailyStocksRepository = $compareDailyStocksRepository;
        $this->likeStockRepository = $likeStockRepository;
        $this->simulationStockBoughtRepository = $simulationStockBoughtRepository;
    }

    /**
     * 總成交量比較
     *
     * @param array $stockNos
     * @param string $date
     * @return array
     * @throws \Exception
     */
    public function compareDailyStocks(array $stockNos, string $date)
    {
        try {
            $compareStocks = $this->stockRepository->getLastTwoWeekDayStockInfo($stockNos, $date);

            foreach ($compareStocks as $compareStock) {
                $stocksNum = Arr::pluck($compareStock, 'stocks');

                // 數量不是兩筆的不做比較
                if (count($stocksNum) !== 2) {
                    continue;
                }
                $stock = Arr::first($compareStock);

                // 次筆比上一筆交易量多兩倍 發告警
                $moreThanThousand = $stocksNum[0] > 2000;
                $isTwiceStocks = $stocksNum[0] > $stocksNum[1] * 2;
                // 漲跌百分比小於8%
//                $isLessThanEightPercent = (floatval($stock['price_diff_percent']) < 8);

                if ($moreThanThousand && $isTwiceStocks) {
                    $compareDailyStock = $this->compareDailyStocksRepository->findByWhere(
                        [
                            'stock_no' => Arr::get($stock, 'stock_no'),
                            'date' => Arr::get($stock, 'date')
                        ]
                    );

                    // 每天只發告警一次
                    $stock['last_stocks'] = $stocksNum[1];
                    if (empty($compareDailyStock)) {
                        Notification::channel(SlackWebhookChannel::class)->sendMessage(
                            new CompareStockNotification($stock)
                        );

                        // 更新 like_stocks 及 simulation_stocks_bought 的 compare_daily_stock 狀態
                        $this->likeStockRepository->updateCompareDailyStockStatus(Arr::get($stock, 'stock_no'));
                        $this->simulationStockBoughtRepository->updateCompareDailyStockStatus(Arr::get($stock, 'stock_no'));
                    }

                    $this->compareDailyStocksRepository->updateOrInsert(
                        [
                            'stock_no' => Arr::get($stock, 'stock_no'),
                            'date' => Arr::get($stock, 'date')
                        ],
                        [
                            'stock_no' => Arr::get($stock, 'stock_no'),
                            'stock_name' => Arr::get($stock, 'stock_name'),
                            'date' => Arr::get($stock, 'date'),
                            'stocks' => Arr::get($stock, 'stocks'),
                            'last_stocks' => Arr::get($stock, 'last_stocks'),
                            'end_price' => Arr::get($stock, 'end_price'),
                            'price_diff' => Arr::get($stock, 'price_diff'),
                            'price_diff_percent' => Arr::get($stock, 'price_diff_percent'),
                        ]
                    );
                }
            }

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 成交倍增股市資訊
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getList(array $params)
    {
        try {
            $compareDailyStocks = $this->compareDailyStocksRepository->getCompareDailyStockLists($params);

            // 檢查股票是否模擬買過
            $compareDailyStocks['data'] = $this->checkSimulationStocks(Arr::get($compareDailyStocks, 'data'));

            // 檢查股票是否加入欲買股票
            $compareDailyStocks['data'] = $this->checkLikeStocks(Arr::get($compareDailyStocks, 'data'));

            return [
                'code' => platform('api_code.success'),
                'data' => [
                    'viewData' => $compareDailyStocks
                ]
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }
}
