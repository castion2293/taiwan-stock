<?php

namespace App\Services;

use Illuminate\Support\Arr;

trait CheckStockTrait
{
    /**
     * 檢查股票是否模擬買過
     *
     * @param array $stocksData
     * @return array
     * @throws \Exception
     */
    public function checkSimulationStocks(array $stocksData): array
    {
        $stockNos = collect($stocksData)->pluck('stock_no')->toArray();

        $simulationStockService = \App::make(SimulationStockService::class);
        $result = $simulationStockService->checkSimulationStocks($stockNos);

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        $checkSimulationStocks = Arr::get($result, 'result');

        return collect($stocksData)->map(
            function ($stock) use ($checkSimulationStocks) {
                $stockNo = Arr::get($stock, 'stock_no');

                return array_merge(
                    $stock,
                    [
                        'bought' => (boolval(Arr::get($checkSimulationStocks, $stockNo)))
                    ]
                );
            }
        )->toArray();
    }

    /**
     * 檢查股票是否加入欲買股票
     *
     * @param array $stocksData
     * @return array
     * @throws \Exception
     */
    public function checkLikeStocks(array $stocksData): array
    {
        $stockNos = collect($stocksData)->pluck('stock_no')->toArray();

        $likeStockService = \App::make(LikeStockService::class);
        $result = $likeStockService->checkLikeStocks($stockNos);

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        $checkLikeStocks = Arr::get($result, 'result');

        return collect($stocksData)->map(
            function ($stock) use ($checkLikeStocks) {
                $stockNo = Arr::get($stock, 'stock_no');
                $likeCheck = (boolval(Arr::get($checkLikeStocks, $stockNo)));

                return array_merge(
                    $stock,
                    [
                        'like' => ($likeCheck) ?
                            platform('status_code.action.removed_like_stock') :
                            platform('status_code.action.like_stock')
                    ]
                );
            }
        )->toArray();
    }
}
