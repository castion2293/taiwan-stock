<?php

namespace App\Services;

use App\Repositories\FiveMinutesStocksRepository;
use Illuminate\Support\Arr;

class FiveMinutesStockService
{
    use CheckStockTrait;

    /**
     * @var FiveMinutesStocksRepository
     */
    protected $fiveMinutesStocksRepository;

    public function __construct(FiveMinutesStocksRepository $fiveMinutesStocksRepository)
    {
        $this->fiveMinutesStocksRepository = $fiveMinutesStocksRepository;
    }

    /**
     * 瞬間巨量股票資訊
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getList(array $params)
    {
        try {
            $fiveMinutesStocks = $this->fiveMinutesStocksRepository->getFiveMinutesStockLists($params);

            // 檢查股票是否模擬買過
            $fiveMinutesStocks['data'] = $this->checkSimulationStocks(Arr::get($fiveMinutesStocks, 'data'));

            // 檢查股票是否加入欲買股票
            $fiveMinutesStocks['data'] = $this->checkLikeStocks(Arr::get($fiveMinutesStocks, 'data'));

            return [
                'code' => platform('api_code.success'),
                'data' => [
                    'viewData' => $fiveMinutesStocks
                ]
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }
}
