<?php

namespace App\Services;

use App\ApiCallers\DividendApiCaller;
use App\Repositories\DividendRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DividendService
{
    /**
     * @var DividendRepository
     */
    protected $dividendRepository;

    /**
     * @var DividendApiCaller
     */
    protected $dividendApiCaller;

    public function __construct(DividendApiCaller $dividendApiCaller, DividendRepository $dividendRepository)
    {
        $this->dividendRepository = $dividendRepository;
        $this->dividendApiCaller = $dividendApiCaller;
    }

    /**
     * 抓取單一股票股利
     *
     * @param string $stockNo
     * @return array
     * @throws \Exception
     */
    public function fetchDividend(string $stockNo): array
    {
        try {
            $dividends = $this->dividendApiCaller->callApi($stockNo);

            DB::transaction(
                function () use ($dividends) {
                    $this->dividendRepository->updateOrCreateMulti($dividends);
                }
            );

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 股票股利資訊
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getList(array $params): array
    {
        try {
            $dividends = $this->dividendRepository->getDividendList($params);

            return [
                'code' => platform('api_code.success'),
                'data' => [
                    'viewData' => $dividends
                ]
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }
}
