<?php

namespace App\Services;

use App\Channels\SlackWebhookChannel;
use App\Crawlers\StockInfoCrawler;
use App\Notifications\AttentionStockNotification;
use App\Notifications\StocksNotification;
use App\Repositories\AttentionStockRepository;
use App\Repositories\FiveMinutesStocksRepository;
use App\Repositories\StockRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class StockService
{
    use CheckStockTrait;

    /**
     * @var StockInfoCrawler
     */
    protected $stockInfoCrawler;

    /**
     * @var StockRepository
     */
    protected $stockRepository;

    /**
     * @var FiveMinutesStocksRepository
     */
    protected $fiveMinutesStocksRepository;

    /**
     * @var AttentionStockRepository
     */
    protected $attentionStockRepository;

    public function __construct(
        StockInfoCrawler $stockInfoCrawler,
        StockRepository $stockRepository,
        FiveMinutesStocksRepository $fiveMinutesStocksRepository,
        AttentionStockRepository $attentionStockRepository
    ) {
        $this->stockInfoCrawler = $stockInfoCrawler;
        $this->stockRepository = $stockRepository;
        $this->fiveMinutesStocksRepository = $fiveMinutesStocksRepository;
        $this->attentionStockRepository = $attentionStockRepository;
    }

    /**
     * 抓取某股票的資訊內容
     * @param string $stockNo
     * @return array
     * @throws \Exception
     */
    public function fetchStockInfo(string $stockNo)
    {
        try {
            $stock = $this->stockInfoCrawler->crawler($stockNo);

            DB::transaction(
                function () use ($stock) {
                    $this->stockRepository->updateOrInsert(
                        [
                            'stock_no' => Arr::get($stock, 'stock_no'),
                            'date' => Arr::get($stock, 'date')
                        ],
                        $stock
                    );
                }
            );

            // 交易量檢查
            $this->checkStocks($stock);

            // 檢查是否是關注股票
            $this->checkAttentionStock($stock);

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 今日行情股市資訊
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getList(array $params)
    {
        try {
            $stocks = $this->stockRepository->getStockLists($params);

            // 檢查股票是否模擬買過
            $stocks['data'] = $this->checkSimulationStocks(Arr::get($stocks, 'data'));

            // 檢查股票是否加入欲買股票
            $stocks['data'] = $this->checkLikeStocks(Arr::get($stocks, 'data'));

            return [
                'code' => platform('api_code.success'),
                'data' => [
                    'viewData' => $stocks
                ]
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 交易量檢查
     *
     * @param array $stock
     */
    private function checkStocks(array $stock)
    {
        if (!Cache::tags(['stocks'])->has($stock['stock_no'])) {
            Cache::tags(['stocks'])->put(
                $stock['stock_no'],
                [
                    'last_5_minutes_stocks' => $stock['stocks'],
                    'total_stocks' => $stock['stocks']
                ]
            );
            return;
        }

        $lastStocks = Cache::tags(['stocks'])->get($stock['stock_no']);
        $lastTotalStocks = Arr::get($lastStocks, 'total_stocks');
        $lastFiveMinutesStocks = Arr::get($lastStocks, 'last_5_minutes_stocks');
        $stock['new_five_minutes_stocks'] = $newFiveMinutesStocks = $stock['stocks'] - $lastTotalStocks;

        // 交易量異常 發訊息通知
        $isTotalStocksGreaterThanThousand = ($stock['stocks'] > 2000) && ($newFiveMinutesStocks > 1000);
        $isTimesStocks = ($newFiveMinutesStocks > $lastFiveMinutesStocks * 3);
        $isTenPercentTodayStocks = ($newFiveMinutesStocks > $lastTotalStocks * 0.35);
        if ($isTotalStocksGreaterThanThousand && $isTimesStocks && $isTenPercentTodayStocks) {
            $fiveMinutesStock = $this->fiveMinutesStocksRepository->findByWhere(
                [
                    'stock_no' => $stock['stock_no'],
                    'date' => $stock['date']
                ]
            );

            // 每天只寫入DB一次並發告警
            if (empty($fiveMinutesStock)) {
                $this->fiveMinutesStocksRepository->store($stock);
                Notification::channel(SlackWebhookChannel::class)->sendMessage(new StocksNotification($stock));
            }
        }

        Cache::tags(['stocks'])->put(
            $stock['stock_no'],
            [
                'last_5_minutes_stocks' => $newFiveMinutesStocks,
                'total_stocks' => $stock['stocks']
            ]
        );
    }

    /**
     * 關注股票檢查
     *
     * @param array $stock
     * @throws \Exception
     */
    private function checkAttentionStock(array $stock)
    {
        $stockNo = Arr::get($stock, 'stock_no');

        $attentionStocks = $this->attentionStockRepository->findByWhere(
            [
                ['stock_no', '=', $stockNo],
                ['status', '=', platform('status_code.attention_stocks.attending')]
            ],
            [
                'id',
                'target_price',
                'greater'
            ]
        );

        if (empty($attentionStocks)) {
            return;
        }

        $needChangeAttentionStockIds = [];
        foreach ($attentionStocks as $attentionStock) {
            $targetPrice = floatval(Arr::get($attentionStock, 'target_price'));
            $endPrice = floatval(Arr::get($stock, 'end_price'));
            $greater = boolval(Arr::get($attentionStock, 'greater'));

            // 大於目標價 = 目前價格 > 目標價 / 小於目標價 = 目前價格 < 目標價
            $attentionCheckResult = ($greater) ? $endPrice > $targetPrice : $endPrice < $targetPrice;

            if ($attentionCheckResult === false) {
                continue;
            }

            $needChangeAttentionStockIds[] = Arr::get($attentionStock, 'id');

            // 發告警
            Notification::channel(SlackWebhookChannel::class)->sendMessage(
                new AttentionStockNotification(
                    [
                        'stock_no' => $stockNo,
                        'stock_name' => Arr::get($stock, 'stock_name'),
                        'target_price' => $targetPrice,
                        'greater' => $greater,
                        'end_price' => $endPrice
                    ]
                )
            );
        }

        // 修改attention_stocks status = 2 已達標

        $this->attentionStockRepository->updateMuti(
            $needChangeAttentionStockIds,
            [
                'status' => platform('status_code.attention_stocks.attended'),
                'attended_at' => now()->toDateTimeString()
            ]
        );
    }
}
