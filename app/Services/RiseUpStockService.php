<?php

namespace App\Services;

use App\Crawlers\GoldenCrossCrawler;
use App\Repositories\RiseUpStockRepository;
use App\Repositories\StockRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RiseUpStockService
{
    use CheckStockTrait;

    /**
     * @var StockRepository
     */
    protected $stockRepository;

    /**
     * @var RiseUpStockRepository
     */
    protected $riseUpStockRepository;

    /**
     * @var GoldenCrossCrawler
     */
    protected $goldenCrossCrawler;

    public function __construct(
        StockRepository $stockRepository,
        RiseUpStockRepository $riseUpStockRepository,
        GoldenCrossCrawler $goldenCrossCrawler
    ) {
        $this->stockRepository = $stockRepository;
        $this->riseUpStockRepository = $riseUpStockRepository;
        $this->goldenCrossCrawler = $goldenCrossCrawler;
    }

    /**
     * 計算日期區間內連率上漲天數
     *
     * @param array $stockNos
     * @param string $startDate
     * @param string $endDate
     * @return array
     * @throws \Exception
     */
    public function calculateRiseUpStocks(array $stockNos, string $startDate, string $endDate): array
    {
        try {
            $riseUpStocks = $this->stockRepository->getRiseUpStocksForOneMonth($stockNos, $startDate, $endDate);

            $riseUpStocks = collect($riseUpStocks)->map(
                function ($stocks, $stockNo) use ($startDate, $endDate) {
                    $stockName = Arr::first($stocks)['stock_name'];

                    // 連續上漲天數陣列
                    $riseUpDaysArray = [];
                    $riseUpSection = 0;
                    $riseUpDaysArray[$riseUpSection] = 0;

                    // 連續紅K天數陣列
                    $redKDaysArray = [];
                    $redKSection = 0;
                    $redKDaysArray[$redKSection] = 0;

                    foreach ($stocks as $stock) {
                        // 計算連續上漲天數
                        $priceDiff = Arr::get($stock, 'price_diff');

                        if ($priceDiff >= 0) {
                            $riseUpDaysArray[$riseUpSection]++;
                        }

                        if ($priceDiff < 0) {
                            $riseUpSection++;
                            $riseUpDaysArray[$riseUpSection] = 0;
                        }

                        // 計算連續紅K天數
                        $todayPriceDiff = floatval(Arr::get($stock, 'today_price_diff'));

                        if ($todayPriceDiff >= 0) {
                            $redKDaysArray[$redKSection]++;
                        }

                        if ($todayPriceDiff < 0) {
                            $redKSection++;
                            $redKDaysArray[$redKSection] = 0;
                        }
                    }

                    /**
                     * 計算連續上漲天數
                     */
                    // 最近連續上漲天數
                    $recentRiseUpDays = Arr::first($riseUpDaysArray);

                    // 最長連續上漲天數
                    $longestRiseUpDays = 0;

                    // 整月連續上漲天數
                    $monthlyRiseUpDays = 0;

                    foreach ($riseUpDaysArray as $days) {
                        if ($days > $longestRiseUpDays) {
                            $longestRiseUpDays = $days;
                        }

                        $monthlyRiseUpDays += $days;
                    }

                    /**
                     * 計算連續紅K天數
                     */
                    // 最近連續紅K天數
                    $recentRedKDays = Arr::first($redKDaysArray);

                    // 最長連續紅K天數
                    $longestRedKDays = 0;

                    // 整月紅K天數
                    $monthlyRedKDays = 0;

                    foreach ($redKDaysArray as $kDays) {
                        if ($kDays > $longestRedKDays) {
                            $longestRedKDays = $kDays;
                        }

                        $monthlyRedKDays += $kDays;
                    }

                    return [
                        'stock_no' => Str::wrap($stockNo, '\''),
                        'stock_name' => Str::wrap($stockName, '\''),
                        'start_date' => Str::wrap($startDate, '\''),
                        'end_date' => Str::wrap($endDate, '\''),
                        'recent_rise_up_days' => $recentRiseUpDays,
                        'longest_rise_up_days' => $longestRiseUpDays,
                        'monthly_rise_up_days' => $monthlyRiseUpDays,
                        'recent_red_k_days' => $recentRedKDays,
                        'longest_red_k_days' => $longestRedKDays,
                        'monthly_red_k_days' => $monthlyRedKDays,
                    ];
                }
            )->toArray();

            DB::transaction(
                function () use ($riseUpStocks) {
                    $this->riseUpStockRepository->updateOrCreateMulti($riseUpStocks);
                }
            );

            return [
                'code' => platform('api_code.success'),
                'error' => '',
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 連續上漲資訊
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getList(array $params): array
    {
        try {
            $riseUpStocks = $this->riseUpStockRepository->getRiseUpStockLists($params);

            // 檢查股票是否模擬買過
            $riseUpStocks['data'] = $this->checkSimulationStocks(Arr::get($riseUpStocks, 'data'));

            // 檢查股票是否加入欲買股票
            $riseUpStocks['data'] = $this->checkLikeStocks(Arr::get($riseUpStocks, 'data'));

            return [
                'code' => platform('api_code.success'),
                'data' => [
                    'viewData' => $riseUpStocks
                ]
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }
}
