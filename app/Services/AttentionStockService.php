<?php

namespace App\Services;

use App\Channels\SlackWebhookChannel;
use App\Notifications\AttentionStockNotification;
use App\Repositories\AttentionStockRepository;
use App\Repositories\StockRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class AttentionStockService
{
    /**
     * @var AttentionStockRepository
     */
    protected $attentionStockRepository;

    /**
     * @var StockRepository
     */
    protected $stockRepository;

    public function __construct(AttentionStockRepository $attentionStockRepository, StockRepository $stockRepository)
    {
        $this->attentionStockRepository = $attentionStockRepository;
        $this->stockRepository = $stockRepository;
    }

    /**
     * 觀注股票資訊
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getList(array $params)
    {
        try {
            $attentionStocks = $this->attentionStockRepository->getAttentionStockList($params);

            // 觀注中 status = 1 需加上 action = stop_attention_stock
            $status = intval(Arr::get($params, 'status'));
            if ($status === platform('status_code.attention_stocks.attending')) {
                $stocksData = Arr::get($attentionStocks, 'data');

                $attentionStocks['data'] = collect($stocksData)->map(
                    function ($stock) {
                        return array_merge(
                            $stock,
                            [
                                'action' => platform('status_code.action.stop_attention')
                            ]
                        );
                    }
                )->toArray();
            }

            return [
                'code' => platform('api_code.success'),
                'data' => [
                    'viewData' => $attentionStocks
                ]
            ];
        } catch (\Exception $exception) {
            return [
                'code' => platform('api_code.notFound'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 建立關注股票
     *
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function store(array $params)
    {
        try {
            $attentionStock = DB::transaction(
                function () use ($params) {
                    $attentionStockParams = [
                        'user_id' => Arr::get($params, 'user_id'),
                        'stock_no' => $stockNo = Arr::get($params, 'stock_no'),
                        'stock_name' => platform("stock_code.{$stockNo}"),
                        'status' => platform('status_code.attention_stocks.attending'),
                        'price' => $this->stockRepository->getLastEndPrice($stockNo, now()->toDateString()),
                        'target_price' => Arr::get($params, 'target_price'),
                        'greater' => Arr::get($params, 'greater')
                    ];

                    return $this->attentionStockRepository->store($attentionStockParams);
                }
            );

            return [
                'code' => platform('api_code.success'),
                'result' => $attentionStock,
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 修改狀態
     *
     * @param array $ids
     * @param int $status
     * @return array
     * @throws \Exception
     */
    public function updateStatus(array $ids, int $status)
    {
        try {
            $result = DB::transaction(
                function () use ($ids, $status) {
                    $this->attentionStockRepository->updateMuti(
                        $ids,
                        [
                            'status' => $status
                        ]
                    );
                }
            );

            return [
                'code' => platform('api_code.success'),
                'result' => $result,
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 檢查觀注股票狀態
     *
     * @return array
     * @throws \Exception
     */
    public function checkAttentionStock()
    {
        try {
            $result = DB::transaction(
                function () {
                    $attentionStocks = $this->attentionStockRepository->getAttentionStockCursor(now()->toDateString());

                    $needChangeAttentionStockIds = [];
                    foreach ($attentionStocks as $attentionStock) {
                        if ($this->attentionStockCheckResult($attentionStock)) {
                            $needChangeAttentionStockIds[] = data_get($attentionStock, 'id');

                            // 發告警
                            Notification::channel(SlackWebhookChannel::class)->sendMessage(
                                new AttentionStockNotification(
                                    [
                                        'stock_no' => data_get($attentionStock, 'stock_no'),
                                        'stock_name' => data_get($attentionStock, 'stock_name'),
                                        'target_price' => data_get($attentionStock, 'target_price'),
                                        'greater' => data_get($attentionStock, 'greater'),
                                        'end_price' => data_get($attentionStock, 'end_price')
                                    ]
                                )
                            );
                        }
                    }

                    if (empty($needChangeAttentionStockIds)) {
                        return;
                    }

                    $this->attentionStockRepository->updateMuti(
                        $needChangeAttentionStockIds,
                        [
                            'status' => platform('status_code.attention_stocks.attended'),
                            'attended_at' => now()->toDateTimeString()
                        ]
                    );
                }
            );

            return [
                'code' => platform('api_code.success'),
                'result' => $result,
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 獲取股價最新價格
     *
     * @param string $stockNo
     * @param string $date
     * @return array
     * @throws \Exception
     */
    public function getLastEndPrice(string $stockNo, string $date)
    {
        try {
            $price = $this->stockRepository->getLastEndPrice($stockNo, $date);

            return [
                'code' => platform('api_code.success'),
                'data' => [
                    'price' => $price
                ],
            ];
        } catch (\Exception $exception) {
            return [
                'code' => $exception->getCode() ?? platform('api_code.notAPICode'),
                'error' => $exception->getMessage(),
            ];
        }
    }

    /**
     * 關注股票檢查結果
     *
     * @param $attentionStock
     * @return bool
     */
    private function attentionStockCheckResult($attentionStock): bool
    {
        $targetPrice = floatval(data_get($attentionStock, 'target_price'));
        $endPrice = floatval(data_get($attentionStock, 'end_price'));
        $greater = boolval(data_get($attentionStock, 'greater'));

        // 大於目標價
        if ($greater) {
            return $endPrice > $targetPrice;
        }

        // 小於目標價
        return $endPrice < $targetPrice;
    }
}
