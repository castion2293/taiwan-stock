<?php

namespace App\Jobs;

use App\Services\StockService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class FetchStockJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * 股票代碼
     * @var string
     */
    public $stockNo = '';

    /**
     * Job重複嘗試次數
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Job可執行時間
     *
     * @var int
     */
    public $timeout = 30;

    /**
     * Create a new job instance.
     *
     * @param string $stockNo
     */
    public function __construct(string $stockNo)
    {
        $this->stockNo = $stockNo;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $stockService = App::make(StockService::class);

        $result = $stockService->fetchStockInfo($this->stockNo);

        if (Arr::get($result, 'code') !== 200001) {
            throw new \Exception(Arr::get($result, 'error'));
        }
    }
}
