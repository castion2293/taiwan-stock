<?php

namespace App\Presenters;

abstract class AbstractPresenter
{
    /**
     * 根據數值正負判定顏色 (正: 紅色, 負: 綠色)
     *
     * @param $number
     * @return string
     */
    public function color($number)
    {
        $color = 'gray-900';

        if (floatval($number) > 0) {
            $color = 'red-500';
        }

        if (floatval($number) < 0) {
            $color = 'green-500';
        }

        return $color;
    }

    /**
     * 數字格式化顯示
     *
     * @param $number
     * @param int $decimals
     * @param string $dec_point
     * @param string $thousands_sep
     * @return string
     */
    public function numberFormat($number, $decimals = 0, $dec_point = '.', $thousands_sep = ',')
    {
        return number_format($number, $decimals, $dec_point, $thousands_sep);
    }

    /**
     * 判斷排序升降符號
     *
     * @param string|null $sort
     * @return string
     */
    public function sortSign(string $sort = null)
    {
        if ($sort === 'asc') {
            return '<i class="fa fa-angle-down fa-1x" aria-hidden="true"></i>';
        }

        if ($sort === 'desc') {
            return '<i class="fa fa-angle-up fa-1x" aria-hidden="true"></i>';
        }

        return '';
    }
}
