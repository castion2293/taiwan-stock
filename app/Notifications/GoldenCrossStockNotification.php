<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Arr;

class GoldenCrossStockNotification extends Notification
{
    use Queueable;

    /**
     * 均線黃金交叉股票資訊
     * @var array
     */
    protected $goldenCrossStocks = [];

    /**
     * Create a new notification instance.
     *
     * @param array $goldenCrossStocks
     */
    public function __construct(array $goldenCrossStocks)
    {
        $this->goldenCrossStocks = $goldenCrossStocks;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * @param $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable = [])
    {
        return (new SlackMessage)->content(
            "{$this->goldenCrossStocks['stock_name']}({$this->goldenCrossStocks['stock_no']})"
        )
            ->attachment(
                function ($attachment) {
                    $attachment
                        ->title('均線黃金交叉')
                        ->fields(
                            [
                                '交易日' => Arr::get($this->goldenCrossStocks, 'date'),
                                '成交張數' => Arr::get($this->goldenCrossStocks, 'stocks'),
                                '收盤價' => Arr::get($this->goldenCrossStocks, 'end_price'),
                            ]
                        );
                }
            );
    }
}
