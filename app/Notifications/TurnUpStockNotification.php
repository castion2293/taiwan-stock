<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Arr;

class TurnUpStockNotification extends Notification
{
    use Queueable;

    /**
     * 均線反轉上揚股票資訊
     *
     * @var array
     */
    protected $turnUpStocks = [];


    /**
     * Create a new notification instance.
     *
     * @param array $turnUpStocks
     */
    public function __construct(array $turnUpStocks)
    {
        $this->turnUpStocks = $turnUpStocks;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * @param $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable = [])
    {
        return (new SlackMessage)->content(
            "{$this->turnUpStocks['stock_name']}({$this->turnUpStocks['stock_no']})"
        )
            ->attachment(
                function ($attachment) {
                    $attachment
                        ->title('均線反轉上揚')
                        ->fields(
                            [
                                '交易日' => Arr::get($this->turnUpStocks, 'date'),
                                '成交張數' => Arr::get($this->turnUpStocks, 'stocks'),
                                '收盤價' => Arr::get($this->turnUpStocks, 'end_price'),
                            ]
                        );
                }
            );
    }
}
