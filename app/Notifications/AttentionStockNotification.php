<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Arr;

class AttentionStockNotification extends Notification
{
    use Queueable;

    /**
     * 關注股票資訊
     *
     * @var array
     */
    private $stock;

    /**
     * Create a new notification instance.
     *
     * @param array $stock
     */
    public function __construct(array $stock)
    {
        $this->stock = $stock;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * @param $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable = [])
    {
        return (new SlackMessage)
            ->attachment(
                function ($attachment) {
                    $attachment
                        ->title('關注股票')
                        ->content($this->stock['stock_name'] . '(' . $this->stock['stock_no'] . ')'
                              . '目前價格 ' . $this->stock['end_price']
                              . ' 已經' . (($this->stock['greater']) ? '高於' : '低於')
                              . '目標價 ' . $this->stock['target_price']);
                }
            );
    }
}
