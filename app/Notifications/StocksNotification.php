<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Arr;

class StocksNotification extends Notification
{
    use Queueable;

    /**
     * 股票資訊
     * @var array
     */
    protected $stock = [];

    /**
     * Create a new notification instance.
     *
     * @param array $stock
     */
    public function __construct(array $stock)
    {
        $this->stock = $stock;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * @param $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable = [])
    {
        return (new SlackMessage)->content("{$this->stock['stock_name']}({$this->stock['stock_no']})")
            ->attachment(
                function ($attachment) {
                    $attachment
                        ->title('瞬間成交量激增')
                        ->fields(
                            [
                                '五分鐘內成交暴增量' => Arr::get($this->stock, 'new_five_minutes_stocks'),
                                '現在成交量' => Arr::get($this->stock, 'stocks'),
                                '現在價格' => Arr::get($this->stock, 'end_price'),
                                '漲跌價差' => Arr::get($this->stock, 'price_diff'),
                                '漲跌價差百分比' => Arr::get($this->stock, 'price_diff_percent') . '%',
                            ]
                        );
                }
            );
    }
}
