<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class AttentionStockScope extends AbstractBaseScope
{
    protected $extensions = [
        'StockNo',
        'AttendedAtSort',
    ];

    /**
     * 篩選 股票代號
     *
     * @param Builder $builder
     */
    public function addStockNo(Builder $builder)
    {
        $builder->macro('stockNo', function (Builder $builder, array $params) {
            $stockNo = Arr::get($params, 'stock_no');

            return $builder->where($builder->getModel()->getTable() . '.stock_no', $stockNo);
        });
    }

    /**
     * 排序 達標日期
     *
     * @param Builder $builder
     */
    public function addAttendedAtSort(Builder $builder)
    {
        $builder->macro('attendedAtSort', function (Builder $builder, array $params) {
            $attendedAtSort = Arr::get($params, 'attended_at_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.attended_at', $attendedAtSort);
        });
    }
}
