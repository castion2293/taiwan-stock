<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class FiveMinutesStockScope extends AbstractBaseScope
{
    protected $extensions = [
        'StockNo',
        'Date',
        'NewFiveMinutesStocksSort',
        'StocksSort',
        'PriceDiffSort',
        'PriceDiffPercentSort',
    ];

    /**
     * 篩選 股票代號
     *
     * @param Builder $builder
     */
    protected function addStockNo(Builder $builder)
    {
        $builder->macro('stockNo', function (Builder $builder, array $params) {
            $stockNo = Arr::get($params, 'stock_no');

            return $builder->where($builder->getModel()->getTable() . '.stock_no', $stockNo);
        });
    }

    /**
     * 篩選 交易時間
     *
     * @param Builder $builder
     */
    protected function addDate(Builder $builder)
    {
        $builder->macro('date', function (Builder $builder, array $params) {
            $date = Arr::get($params, 'date');

            return $builder->where($builder->getModel()->getTable() . '.date', $date);
        });
    }

    /**
     * 排序 5分鐘內成交暴增量
     *
     * @param Builder $builder
     */
    protected function addNewFiveMinutesStocksSort(Builder $builder)
    {
        $builder->macro('newFiveMinutesStocksSort', function (Builder $builder, array $params) {
            $newFiveMinutesStocksSort = Arr::get($params, 'new_five_minutes_stocks_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.new_five_minutes_stocks', $newFiveMinutesStocksSort);
        });
    }

    /**
     * 排序 現在成交量
     *
     * @param Builder $builder
     */
    protected function addStocksSort(Builder $builder)
    {
        $builder->macro('stocksSort', function (Builder $builder, array $params) {
            $stocksSort = Arr::get($params, 'stocks_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.stocks', $stocksSort);
        });
    }

    /**
     * 排序 漲跌價差
     *
     * @param Builder $builder
     */
    protected function addPriceDiffSort(Builder $builder)
    {
        $builder->macro('priceDiffSort', function (Builder $builder, array $params) {
            $priceDiffSort = Arr::get($params, 'price_diff_sort');

            return$builder->orderBy($builder->getModel()->getTable() . '.price_diff', $priceDiffSort);
        });
    }

    /**
     * 排序 價差百分比
     *
     * @param Builder $builder
     */
    protected function addPriceDiffPercentSort(Builder $builder)
    {
        $builder->macro('priceDiffPercentSort', function (Builder $builder, array $params) {
            $priceDiffPercentSort = Arr::get($params, 'price_diff_percent_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.price_diff_percent', $priceDiffPercentSort);
        });
    }
}
