<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class LikeStockScope extends AbstractBaseScope
{
    protected $extensions = [
        'StockNo',
        'LikeAtSort',
        'EndPriceSort',
        'PriceDiffSort',
        'PriceDiffPercentSort',
    ];

    /**
     * 篩選 股票代號
     *
     * @param Builder $builder
     */
    public function addStockNo(Builder $builder)
    {
        $builder->macro('stockNo', function (Builder $builder, array $params) {
            $stockNo = Arr::get($params, 'stock_no');

            return $builder->where($builder->getModel()->getTable() . '.stock_no', $stockNo);
        });
    }

    /**
     * 排序 加入日期
     *
     * @param Builder $builder
     */
    public function addLikeAtSort(Builder $builder)
    {
        $builder->macro('likeAtSort', function (Builder $builder, array $params) {
            $likeAtSort = Arr::get($params, 'like_at_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.like_at', $likeAtSort);
        });
    }

    /**
     * 排序 現在價格
     *
     * @param Builder $builder
     */
    public function addEndPriceSort(Builder $builder)
    {
        $builder->macro('endPriceSort', function (Builder $builder, array $params) {
            $endPriceSort = Arr::get($params, 'end_price_sort');

            return $builder->orderBy('last_stocks.end_price', $endPriceSort);
        });
    }

    /**
     * 排序 今日漲跌
     *
     * @param Builder $builder
     */
    public function addPriceDiffSort(Builder $builder)
    {
        $builder->macro('priceDiffSort', function (Builder $builder, array $params) {
            $priceDiffSort = Arr::get($params, 'price_diff_sort');

            return $builder->orderBy('last_stocks.price_diff', $priceDiffSort);
        });
    }

    /**
     * 排序 今日漲跌百分比
     *
     * @param Builder $builder
     */
    public function addPriceDiffPercentSort(Builder $builder)
    {
        $builder->macro('priceDiffPercentSort', function (Builder $builder, array $params) {
            $priceDiffPercentSort = Arr::get($params, 'price_diff_percent_sort');

            return $builder->orderBy('last_stocks.price_diff_percent', $priceDiffPercentSort);
        });
    }
}
