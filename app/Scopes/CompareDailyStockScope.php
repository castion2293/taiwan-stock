<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class CompareDailyStockScope extends AbstractBaseScope
{
    protected $extensions = [
        'StockNo',
        'Date',
        'StocksSort',
        'LastStocksSort',
        'EndPriceSort',
        'PriceDiffSort',
        'PriceDiffPercentSort',
        'StocksIncreaseSort',
    ];

    /**
     * 篩選 股票代號
     *
     * @param Builder $builder
     */
    protected function addStockNo(Builder $builder)
    {
        $builder->macro('stockNo', function (Builder $builder, array $params) {
            $stockNo = Arr::get($params, 'stock_no');

            return $builder->where('stock_no', $stockNo);
        });
    }

    /**
     * 篩選 交易時間
     *
     * @param Builder $builder
     */
    protected function addDate(Builder $builder)
    {
        $builder->macro('date', function (Builder $builder, array $params) {
            $date = Arr::get($params, 'date');

            return $builder->where('date', $date);
        });
    }

    /**
     * 排序 現在成交量
     *
     * @param Builder $builder
     */
    protected function addStocksSort(Builder $builder)
    {
        $builder->macro('stocksSort', function (Builder $builder, array $params) {
            $stocksSort = Arr::get($params, 'stocks_sort');

            return $builder->orderBy('stocks', $stocksSort);
        });
    }

    /**
     * 排序 昨日成交量
     *
     * @param Builder $builder
     */
    protected function addLastStocksSort(Builder $builder)
    {
        $builder->macro('lastStocksSort', function (Builder $builder, array $params) {
            $lastStocksSort = Arr::get($params, 'last_stocks_sort');

            return $builder->orderBy('last_stocks', $lastStocksSort);
        });
    }

    /**
     * 排序 現在價格
     *
     * @param Builder $builder
     */
    protected function addEndPriceSort(Builder $builder)
    {
        $builder->macro('endPriceSort', function (Builder $builder, array $params) {
            $endPriceSort = Arr::get($params, 'end_price_sort');

            return $builder->orderBy('end_price', $endPriceSort);
        });
    }

    /**
     * 排序 漲跌價差
     *
     * @param Builder $builder
     */
    protected function addPriceDiffSort(Builder $builder)
    {
        $builder->macro('priceDiffSort', function (Builder $builder, array $params) {
            $priceDiffSort = Arr::get($params, 'price_diff_sort');

            return $builder->orderBy('price_diff', $priceDiffSort);
        });
    }

    /**
     * 排序 價差百分比
     *
     * @param Builder $builder
     */
    protected function addPriceDiffPercentSort(Builder $builder)
    {
        $builder->macro('priceDiffPercentSort', function (Builder $builder, array $params) {
            $priceDiffPercentSort = Arr::get($params, 'price_diff_percent_sort');

            return $builder->orderBy('price_diff_percent', $priceDiffPercentSort);
        });
    }

    /**
     * 排序 成交量倍增
     *
     * @param Builder $builder
     */
    protected function addStocksIncreaseSort(Builder $builder)
    {
        $builder->macro('stocksIncreaseSort', function (Builder $builder, array $params) {
            $stocksIncreaseSort = Arr::get($params, 'stocks_increase_sort');

            return $builder->orderBy('stocks_increase', $stocksIncreaseSort);
        });
    }
}
