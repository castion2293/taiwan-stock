<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class DividendScope extends AbstractBaseScope
{
    protected $extensions = [
        'StockNo',
        'Year',
        'YearSort',
        'DividendSort',
        'TimesSort',
        'DividendPercentSort',
    ];

    /**
     * 篩選 股票代號
     *
     * @param Builder $builder
     */
    public function addStockNo(Builder $builder)
    {
        $builder->macro('stockNo', function (Builder $builder, array $params) {
            $stockNo = Arr::get($params, 'stock_no');

            return $builder->where($builder->getModel()->getTable() . '.stock_no', $stockNo);
        });
    }

    /**
     * 篩選 年份
     *
     * @param Builder $builder
     */
    public function addYear(Builder $builder)
    {
        $builder->macro('year', function (Builder $builder, array $params) {
            $year = Arr::get($params, 'year');

            return $builder->having('year', $year);
        });
    }

    /**
     * 排序 現金股利
     *
     * @param Builder $builder
     */
    public function addDividendSort(Builder $builder)
    {
        $builder->macro('dividendSort', function (Builder $builder, array $params) {
            $dividendSort = Arr::get($params, 'dividend_sort');

            return $builder->orderBy('dividend', $dividendSort);
        });
    }

    /**
     * 排序 發放次數
     *
     * @param Builder $builder
     */
    public function addTimesSort(Builder $builder)
    {
        $builder->macro('timesSort', function (Builder $builder, array $params) {
            $timesSort = Arr::get($params, 'times_sort');

            return $builder->orderBy('times', $timesSort);
        });
    }

    /**
     * 排序 殖利率
     *
     * @param Builder $builder
     */
    public function addDividendPercentSort(Builder $builder)
    {
        $builder->macro('dividendPercentSort', function (Builder $builder, array $params) {
            $dividendPercentSort = Arr::get($params, 'dividend_percent_sort');

            return $builder->orderBy('dividend_percent', $dividendPercentSort);
        });
    }

    /**
     * 排序 年份
     *
     * @param Builder $builder
     */
    public function addYearSort(Builder $builder)
    {
        $builder->macro('yearSort', function (Builder $builder, array $params) {
            $yearSort = Arr::get($params, 'year_sort');

            return $builder->orderBy('year', $yearSort);
        });
    }
}
