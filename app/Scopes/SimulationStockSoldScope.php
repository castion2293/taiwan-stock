<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class SimulationStockSoldScope extends AbstractBaseScope
{
    protected $extensions = [
        'StockNo',
        'SaleDate',
        'SaleStocksSort',
        'BuyPriceSort',
        'SalePriceSort',
        'SaleDateSort',
        'ProfitLossSort',
        'ProfitLossPercentSort',
    ];

    /**
     * 篩選 股票代號
     *
     * @param Builder $builder
     */
    public function addStockNo(Builder $builder)
    {
        $builder->macro('stockNo', function (Builder $builder, array $params) {
            $stockNo = Arr::get($params, 'stock_no');

            return $builder->where($builder->getModel()->getTable() . '.stock_no', $stockNo);
        });
    }

    /**
     * 篩選 販賣日期
     *
     * @param Builder $builder
     */
    public function addSaleDate(Builder $builder)
    {
        $builder->macro('saleDate', function (Builder $builder, array $params) {
            $saleDate = Arr::get($params, 'sale_date');

            return $builder->where($builder->getModel()->getTable() . '.sale_date', $saleDate);
        });
    }

    /**
     * 排序 販賣張數
     *
     * @param Builder $builder
     */
    public function addSaleStocksSort(Builder $builder)
    {
        $builder->macro('saleStocksSort', function (Builder $builder, array $params) {
            $saleStocksSort = Arr::get($params, 'sale_stocks_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.sale_stocks', $saleStocksSort);
        });
    }

    /**
     * 排序 購買成交均價
     *
     * @param Builder $builder
     */
    public function addBuyPriceSort(Builder $builder)
    {
        $builder->macro('buyPriceSort', function (Builder $builder, array $params) {
            $buyPriceSort = Arr::get($params, 'buy_price_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.buy_price', $buyPriceSort);
        });
    }

    /**
     * 排序 販賣價格
     *
     * @param Builder $builder
     */
    public function addSalePriceSort(Builder $builder)
    {
        $builder->macro('salePriceSort', function (Builder $builder, array $params) {
            $salePriceSort = Arr::get($params, 'sale_price_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.sale_price', $salePriceSort);
        });
    }

    /**
     * 排序 販賣日期
     *
     * @param Builder $builder
     */
    public function addSaleDateSort(Builder $builder)
    {
        $builder->macro('saleDateSort', function (Builder $builder, array $params) {
            $saleDateSort = Arr::get($params, 'sale_date_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.sale_date', $saleDateSort);
        });
    }

    /**
     * 排序 損益
     *
     * @param Builder $builder
     */
    public function addProfitLossSort(Builder $builder)
    {
        $builder->macro('profitLossSort', function (Builder $builder, array $params) {
            $profitLossSort = Arr::get($params, 'profit_loss_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.profit_loss', $profitLossSort);
        });
    }

    /**
     * 排序 報酬率
     *
     * @param Builder $builder
     */
    public function addProfitLossPercentSort(Builder $builder)
    {
        $builder->macro('profitLossPercentSort', function (Builder $builder, array $params) {
            $profitLossPercentSort = Arr::get($params, 'profit_loss_percent_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.profit_loss_percent', $profitLossPercentSort);
        });
    }
}
