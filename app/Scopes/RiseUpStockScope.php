<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class RiseUpStockScope extends AbstractBaseScope
{
    protected $extensions = [
        'StockNo',
        'EndDate',
        'RecentRiseUpDays',
        'RecentRedKDays',
        'RecentRiseUpDaysSort',
        'RecentRedKDaysSort',
        'LongestRiseUpDaysSort',
        'LongestRedKDaysSort',
        'MonthlyRiseUpDaysSort',
        'MonthlyRedKDaysSort',
    ];

    /**
     * 篩選 股票代號時間
     *
     * @param Builder $builder
     */
    protected function addStockNo(Builder $builder)
    {
        $builder->macro('stockNo', function (Builder $builder, array $params) {
            $stockNo = Arr::get($params, 'stock_no');

            return $builder->where($builder->getModel()->getTable() . '.stock_no', $stockNo);
        });
    }

    /**
     * 篩選 結束交易時間
     *
     * @param Builder $builder
     */
    protected function addEndDate(Builder $builder)
    {
        $builder->macro('endDate', function (Builder $builder, array $params) {
            $endDate = Arr::get($params, 'end_date');

            return $builder->where('end_date', $endDate);
        });
    }

    /**
     * 篩選 最近連續上漲天數
     *
     * @param Builder $builder
     */
    protected function addRecentRiseUpDays(Builder $builder)
    {
        $builder->macro('recentRiseUpDays', function (Builder $builder, array $params) {
            $recentRiseUpDays = Arr::get($params, 'recent_rise_up_days');

            return $builder->where('recent_rise_up_days', '>=', $recentRiseUpDays);
        });
    }

    /**
     * 篩選 最近連續紅K天數
     *
     * @param Builder $builder
     */
    protected function addRecentRedKDays(Builder $builder)
    {
        $builder->macro('recentRedKDays', function (Builder $builder, array $params) {
            $recentRedKDays = Arr::get($params, 'recent_red_k_days');

            return $builder->where('recent_red_k_days', '>=', $recentRedKDays);
        });
    }

    /**
     * 排序 最近連續上漲
     *
     * @param Builder $builder
     */
    protected function addRecentRiseUpDaysSort(Builder $builder)
    {
        $builder->macro('recentRiseUpDaysSort', function (Builder $builder, array $params) {
            $recentRiseUpDaysSort = Arr::get($params, 'recent_rise_up_days_sort');

            return $builder->orderBy('recent_rise_up_days', $recentRiseUpDaysSort);
        });
    }

    /**
     * 排序 最近連續紅K
     *
     * @param Builder $builder
     */
    protected function addRecentRedKDaysSort(Builder $builder)
    {
        $builder->macro('recentRedKDaysSort', function (Builder $builder, array $params) {
            $recentRedKDaysSort = Arr::get($params, 'recent_red_k_days_sort');

            return $builder->orderBy('recent_red_k_days', $recentRedKDaysSort);
        });
    }

    /**
     * 排序 最長連續上漲
     *
     * @param Builder $builder
     */
    protected function addLongestRiseUpDaysSort(Builder $builder)
    {
        $builder->macro('longestRiseUpDaysSort', function (Builder $builder, array $params) {
            $longestRiseUpDaysSort = Arr::get($params, 'longest_rise_up_days_sort');

            return $builder->orderBy('longest_rise_up_days', $longestRiseUpDaysSort);
        });
    }

    /**
     * 排序 最長連續紅K
     *
     * @param Builder $builder
     */
    protected function addLongestRedKDaysSort(Builder $builder)
    {
        $builder->macro('longestRedKDaysSort', function (Builder $builder, array $params) {
            $LongestRedKDaysSort = Arr::get($params, 'longest_red_k_days_sort');

            return $builder->orderBy('longest_red_k_days', $LongestRedKDaysSort);
        });
    }

    /**
     * 排序 整月上漲天數
     *
     * @param Builder $builder
     */
    protected function addMonthlyRiseUpDaysSort(Builder $builder)
    {
        $builder->macro('monthlyRiseUpDaysSort', function (Builder $builder, array $params) {
            $monthlyRiseUpDaysSort = Arr::get($params, 'monthly_rise_up_days_sort');

            return $builder->orderBy('monthly_rise_up_days', $monthlyRiseUpDaysSort);
        });
    }

    /**
     * 排序 整月紅K天數
     *
     * @param Builder $builder
     */
    protected function addMonthlyRedKDaysSort(Builder $builder)
    {
        $builder->macro('monthlyRedKDaysSort', function (Builder $builder, array $params) {
            $monthlyRedKDaysSort = Arr::get($params, 'monthly_red_k_days_sort');

            return $builder->orderBy('monthly_red_k_days', $monthlyRedKDaysSort);
        });
    }
}
