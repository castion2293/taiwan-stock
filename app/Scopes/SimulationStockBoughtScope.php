<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class SimulationStockBoughtScope extends AbstractBaseScope
{
    protected $extensions = [
        'StockNo',
        'BuyDate',
        'BuyStocksSort',
        'BuyDateSort',
        'EndPriceSort',
        'PriceDiffSort',
        'PriceDiffPercentSort',
        'TodayPriceDiffSort',
    ];

    /**
     * 篩選 股票代號
     *
     * @param Builder $builder
     */
    public function addStockNo(Builder $builder)
    {
        $builder->macro('stockNo', function (Builder $builder, array $params) {
            $stockNo = Arr::get($params, 'stock_no');

            return $builder->where($builder->getModel()->getTable() . '.stock_no', $stockNo);
        });
    }

    /**
     * 篩選 購買日期
     *
     * @param Builder $builder
     */
    public function addBuyDate(Builder $builder)
    {
        $builder->macro('buyDate', function (Builder $builder, array $params) {
            $buyDate = Arr::get($params, 'buy_date');

            return $builder->where($builder->getModel()->getTable() . '.buy_date', $buyDate);
        });
    }

    /**
     * 排序 買入張數
     *
     * @param Builder $builder
     */
    public function addBuyStocksSort(Builder $builder)
    {
        $builder->macro('buyStocksSort', function (Builder $builder, array $params) {
            $buyStocksSort = Arr::get($params, 'buy_stocks_sort');

            $table = $builder->getModel()->getTable();
            return $builder->orderByRaw("({$table}.buy_stocks - {$table}.sale_stocks) {$buyStocksSort}");
        });
    }

    /**
     * 排序 購買日期
     *
     * @param Builder $builder
     */
    public function addBuyDateSort(Builder $builder)
    {
        $builder->macro('buyDateSort', function (Builder $builder, array $params) {
            $buyDateSort = Arr::get($params, 'buy_date_sort');

            return $builder->orderBy($builder->getModel()->getTable() . '.buy_date', $buyDateSort);
        });
    }

    /**
     * 排序 現在價格
     *
     * @param Builder $builder
     */
    public function addEndPriceSort(Builder $builder)
    {
        $builder->macro('endPriceSort', function (Builder $builder, array $params) {
            $endPriceSort = Arr::get($params, 'end_price_sort');

            return $builder->orderBy('last_stocks.end_price', $endPriceSort);
        });
    }

    /**
     * 排序 損益
     *
     * @param Builder $builder
     */
    public function addPriceDiffSort(Builder $builder)
    {
        $builder->macro('priceDiffSort', function (Builder $builder, array $params) {
            $priceDiffSort = Arr::get($params, 'price_diff_sort');

            return $builder->orderBy('price_diff', $priceDiffSort);
        });
    }

    /**
     * 排序 報酬率
     *
     * @param Builder $builder
     */
    public function addPriceDiffPercentSort(Builder $builder)
    {
        $builder->macro('priceDiffPercentSort', function (Builder $builder, array $params) {
            $priceDiffPercentSort = Arr::get($params, 'price_diff_percent_sort');

            return $builder->orderBy('price_diff_percent', $priceDiffPercentSort);
        });
    }

    /**
     * 排序 今日漲跌
     *
     * @param Builder $builder
     */
    public function addTodayPriceDiffSort(Builder $builder)
    {
        $builder->macro('todayPriceDiffSort', function (Builder $builder, array $params) {
            $todayPriceDiffSort = Arr::get($params, 'today_price_diff_sort');

            return $builder->orderBy('today_price_diff', $todayPriceDiffSort);
        });
    }
}
