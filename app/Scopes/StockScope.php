<?php


namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class StockScope extends AbstractBaseScope
{
    protected $extensions = [
        'Date',
        'StockNo',
        'StocksSort',
        'EndPriceSort',
        'LastEndPriceSort',
        'PriceDiffSort',
        'PriceDiffPercentSort',
        'current',
    ];

    /**
     * 篩選 交易時間
     *
     * @param Builder $builder
     */
    protected function addDate(Builder $builder)
    {
        $builder->macro('date', function (Builder $builder, array $params) {
            $date = Arr::get($params, 'date');

            return $builder->where('date', $date);
        });
    }

    /**
     * 篩選 股票代號
     *
     * @param Builder $builder
     */
    protected function addStockNo(Builder $builder)
    {
        $builder->macro('stockNo', function (Builder $builder, array $params) {
            $stockNo = Arr::get($params, 'stock_no');

            return $builder->where('stock_no', $stockNo);
        });
    }

    /**
     * 排序 成交張數
     *
     * @param Builder $builder
     */
    protected function addStocksSort(Builder $builder)
    {
        $builder->macro('stocksSort', function (Builder $builder, array $params) {
            $stocksSort = Arr::get($params, 'stocks_sort');

            return $builder->orderBy('stocks', $stocksSort);
        });
    }

    /**
     * 排序 目前價格
     *
     * @param Builder $builder
     */
    protected function addEndPriceSort(Builder $builder)
    {
        $builder->macro('endPriceSort', function (Builder $builder, array $params) {
            $endPriceSort = Arr::get($params, 'end_price_sort');

            return $builder->orderBy('end_price', $endPriceSort);
        });
    }

    /**
     * 排序 昨日價格
     *
     * @param Builder $builder
     */
    protected function addLastEndPriceSort(Builder $builder)
    {
        $builder->macro('lastEndPriceSort', function (Builder $builder, array $params) {
            $endPriceSort = Arr::get($params, 'last_end_price_sort');

            return $builder->orderBy('last_end_price', $endPriceSort);
        });
    }

    /**
     * 排序 漲跌價差
     *
     * @param Builder $builder
     */
    protected function addPriceDiffSort(Builder $builder)
    {
        $builder->macro('priceDiffSort', function (Builder $builder, array $params) {
            $endPriceSort = Arr::get($params, 'price_diff_sort');

            return $builder->orderBy('price_diff', $endPriceSort);
        });
    }

    /**
     * 排序 價差百分比
     *
     * @param Builder $builder
     */
    protected function addPriceDiffPercentSort(Builder $builder)
    {
        $builder->macro('priceDiffPercentSort', function (Builder $builder, array $params) {
            $endPriceSort = Arr::get($params, 'price_diff_percent_sort');

            return $builder->orderBy('price_diff_percent', $endPriceSort);
        });
    }

    /**
     * 獲取各股票最新一筆資訊
     *
     * @param Builder $builder
     */
    protected function addCurrent(Builder $builder)
    {
        $builder->macro('current', function (Builder $builder, array $params) {
            $maxIdsQuery = $builder->selectRaw('MAX(id) AS max_id')
                ->groupBy('stock_no');

            return $builder->whereRaw("id IN ({$maxIdsQuery->toSql()})");
        });
    }
}
