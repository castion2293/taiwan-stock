<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    /**
     * key自動增值
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * PK
     *
     * @var string
     */
    protected $primaryKey = 'key';

    /**
     * @var string[]
     */
    protected $fillable = ['key', 'value', 'raw'];

    /**
     * @var string[]
     */
    protected $casts = [
        'value' => 'array'
    ];
}
