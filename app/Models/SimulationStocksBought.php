<?php

namespace App\Models;

use App\Scopes\SimulationStockBoughtScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SimulationStocksBought extends Model
{
    use HasFactory;

    protected $table = 'simulation_stocks_bought';

    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     */
    protected static function booted()
    {
        static::addGlobalScope(new SimulationStockBoughtScope);
    }

    protected $casts = [
        'buy_price' => 'float',
        'all_sold' => 'boolean',
        'death_cross' => 'boolean',
        'turn_down' => 'boolean',
        'gold_cross' => 'boolean',
        'turn_up' => 'boolean',
        'compare_daily_stock' => 'boolean'
    ];
}
