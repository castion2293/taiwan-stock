<?php

namespace App\Models;

use App\Scopes\AttentionStockScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttentionStock extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'greater' => 'boolean'
    ];

    /**
     * The "booted" method of the model.
     */
    protected static function booted()
    {
        static::addGlobalScope(new AttentionStockScope());
    }
}
