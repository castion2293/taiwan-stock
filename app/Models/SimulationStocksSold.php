<?php

namespace App\Models;

use App\Scopes\SimulationStockSoldScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SimulationStocksSold extends Model
{
    use HasFactory;

    protected $table = 'simulation_stocks_sold';

    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     */
    protected static function booted()
    {
        static::addGlobalScope(new SimulationStockSoldScope);
    }
}
