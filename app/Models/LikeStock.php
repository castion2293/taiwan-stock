<?php

namespace App\Models;

use App\Scopes\LikeStockScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LikeStock extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     */
    protected static function booted()
    {
        static::addGlobalScope(new LikeStockScope);
    }

    protected $casts = [
        'gold_cross' => 'boolean',
        'turn_up' => 'boolean',
        'compare_daily_stock' => 'boolean'
    ];
}
