<?php

namespace App\Models;

use App\Scopes\FiveMinutesStockScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FiveMinutesStocks extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     */
    protected static function booted()
    {
        static::addGlobalScope(new FiveMinutesStockScope);
    }
}
