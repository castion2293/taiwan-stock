<?php

namespace App\Models;

use App\Scopes\StockScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    use HasFactory;

    /**
     * The "booted" method of the model.
     */
    protected static function booted()
    {
        static::addGlobalScope(new StockScope);
    }

    protected $casts = [
        'end_price' => 'float',
        'last_end_price' => 'float',
        'price_diff' => 'float'
    ];
}
