<?php

namespace App\Models;

use App\Scopes\RiseUpStockScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiseUpStock extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * The "booted" method of the model.
     */
    protected static function booted()
    {
        static::addGlobalScope(new RiseUpStockScope);
    }

    protected $casts = [
        'gold_cross' => 'boolean',
        'turn_up' => 'boolean',
    ];
}
