<?php

namespace App\Console\Commands;

use App\Jobs\FetchStockJob;
use App\Services\StockService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class FetchStockCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:stock {--stock_no=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '抓取單日該股票的資訊';
    /**
     * @var StockService
     */
    protected $stockService;

    /**
     * Create a new command instance.
     *
     * @param StockService $stockService
     */
    public function __construct(StockService $stockService)
    {
        parent::__construct();

        $this->stockService = $stockService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $stockNo = $this->option('stock_no');

            // 沒有輸入股票代碼就抓取所有股票資訊
            if (empty($stockNo)) {
                $this->fetchAllStocks();
                $this->info('已部署成功');
                return 0;
            }

            $result = $this->stockService->fetchStockInfo($stockNo);

            if (Arr::get($result, 'code') !== platform('api_code.success')) {
                throw new \Exception(Arr::get($result, 'error'));
            }

            $this->info('已抓取成功');
            return 0;
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
            throw $exception;
        }
    }

    /**
     * 抓取所有的股票資訊
     */
    private function fetchAllStocks()
    {
        $allStockNos = array_keys(platform('stock_code'));

        foreach ($allStockNos as $stockNo) {
            dispatch(new FetchStockJob($stockNo))->onQueue('stock');
        }
    }
}
