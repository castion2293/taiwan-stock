<?php

namespace App\Console\Commands;

use App\Services\AttentionStockService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CheckAttentionStockCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:attention';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '檢查關注股票達標情形';

    /**
     * @var AttentionStockService
     */
    protected $attentionStockService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AttentionStockService $attentionStockService)
    {
        parent::__construct();
        $this->attentionStockService = $attentionStockService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        $result = $this->attentionStockService->checkAttentionStock();

        if (Arr::get($result, 'code') !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        $this->info('檢查完畢');

        return 0;
    }
}
