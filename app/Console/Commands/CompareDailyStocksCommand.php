<?php

namespace App\Console\Commands;

use App\Services\CompareDailyStockService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CompareDailyStocksCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'compare:daily-stocks {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '當日交易量與前一日交易量比較';

    /**
     * @var CompareDailyStockService
     */
    protected $compareDailyStockService;

    /**
     * Create a new command instance.
     *
     * @param CompareDailyStockService $compareDailyStockService
     */
    public function __construct(CompareDailyStockService $compareDailyStockService)
    {
        parent::__construct();
        $this->compareDailyStockService = $compareDailyStockService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        $date = $this->option('date');
        if (empty($date)) {
            $date = today()->toDateString();
        }

        $allStockNos = array_keys(platform('stock_code'));

        $bar = $this->output->createProgressBar(count($allStockNos));
        $bar->start();

        $allStockNoChunks = collect($allStockNos)->chunk(50)->toArray();
        foreach ($allStockNoChunks as $allStockNoChunk) {
            $result = $this->compareDailyStockService->compareDailyStocks($allStockNoChunk, $date);

            if (Arr::get($result, 'code') !== platform('api_code.success')) {
                $this->error(Arr::get($result, 'error'));
            }

            $bar->advance(50);
        }

        $bar->finish();

        $this->info('檢查完畢');
        return 0;
    }
}
