<?php

namespace App\Console\Commands;

use App\Jobs\FetchDividendJob;
use App\Services\DividendService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class FetchDividendCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:dividend {--stock_no=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '抓取該股票的股利資訊';
    /**
     * @var DividendService
     */
    protected $dividendService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DividendService $dividendService)
    {
        parent::__construct();

        $this->dividendService = $dividendService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        $stockNo = $this->option('stock_no');

        // 沒有輸入股票代碼就抓取所有股票股利資訊
        if (empty($stockNo)) {
            $this->fetchAllDividends();
            $this->info('已部署成功');
            return 0;
        }

        $result = $this->dividendService->fetchDividend($stockNo);

        if (Arr::get($result, 'code') !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        $this->info('已抓取成功');
        return 0;
    }

    /**
     * 抓取所有的股票股利資訊
     */
    private function fetchAllDividends()
    {
        $allStockNos = array_keys(platform('stock_code'));

        foreach ($allStockNos as $stockNo) {
            dispatch(new FetchDividendJob($stockNo))->onQueue('stock');
        }
    }
}
