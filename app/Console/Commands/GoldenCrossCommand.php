<?php

namespace App\Console\Commands;

use App\Services\CrossStockService;
use App\Services\RiseUpStockService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class GoldenCrossCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:golden-cross';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '爬蟲抓取均線黃金交叉股票';

    /**
     * @var CrossStockService
     */
    protected $crossStockService;

    /**
     * Create a new command instance.
     *
     * @param CrossStockService $crossStockService
     */
    public function __construct(CrossStockService $crossStockService)
    {
        parent::__construct();

        $this->crossStockService = $crossStockService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        $result = $this->crossStockService->fetchGoldenCross();

        if (Arr::get($result, 'code') !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        $this->info('已抓取成功');
        return 0;
    }
}
