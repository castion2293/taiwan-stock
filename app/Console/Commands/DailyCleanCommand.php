<?php

namespace App\Console\Commands;

use App\Repositories\AttentionStockRepository;
use App\Repositories\CompareDailyStocksRepository;
use App\Repositories\FiveMinutesStocksRepository;
use App\Repositories\LikeStockRepository;
use App\Repositories\RiseUpStockRepository;
use App\Repositories\SimulationStockBoughtRepository;
use App\Repositories\SimulationStockSoldRepository;
use App\Repositories\StockRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DailyCleanCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:clean {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每週固定清除任務';

    /**
     * @var AttentionStockRepository
     */
    protected $attentionStockRepository;

    /**
     * @var CompareDailyStocksRepository
     */
    protected $compareDailyStocksRepository;

    /**
     * @var FiveMinutesStocksRepository
     */
    protected $fiveMinutesStocksRepository;

    /**
     * @var LikeStockRepository
     */
    protected $likeStockRepository;

    /**
     * @var RiseUpStockRepository
     */
    protected $riseUpStockRepository;

    /**
     * @var StockRepository
     */
    protected $stockRepository;

    /**
     * @var SimulationStockBoughtRepository
     */
    protected $simulationStockBoughtRepository;

    /**
     * @var SimulationStockSoldRepository
     */
    protected $simulationStockSoldRepository;

    /**
     * Create a new command instance.
     *
     * @param AttentionStockRepository $attentionStockRepository
     * @param CompareDailyStocksRepository $compareDailyStocksRepository
     * @param FiveMinutesStocksRepository $fiveMinutesStocksRepository
     * @param LikeStockRepository $likeStockRepository
     * @param RiseUpStockRepository $riseUpStockRepository
     * @param SimulationStockBoughtRepository $simulationStockBoughtRepository
     * @param SimulationStockSoldRepository $simulationStockSoldRepository
     * @param StockRepository $stockRepository
     */
    public function __construct(
        AttentionStockRepository $attentionStockRepository,
        CompareDailyStocksRepository $compareDailyStocksRepository,
        FiveMinutesStocksRepository $fiveMinutesStocksRepository,
        LikeStockRepository $likeStockRepository,
        RiseUpStockRepository $riseUpStockRepository,
        SimulationStockBoughtRepository $simulationStockBoughtRepository,
        SimulationStockSoldRepository $simulationStockSoldRepository,
        StockRepository $stockRepository
    ) {
        parent::__construct();
        $this->attentionStockRepository = $attentionStockRepository;
        $this->compareDailyStocksRepository = $compareDailyStocksRepository;
        $this->fiveMinutesStocksRepository = $fiveMinutesStocksRepository;
        $this->likeStockRepository = $likeStockRepository;
        $this->riseUpStockRepository = $riseUpStockRepository;
        $this->stockRepository = $stockRepository;
        $this->simulationStockBoughtRepository = $simulationStockBoughtRepository;
        $this->simulationStockSoldRepository = $simulationStockSoldRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = $this->option('date');
        if (empty($date)) {
            $date = now()->toDateString();
        }

        // 清除 failed_jobs
        $this->cleanFailJobs();

        // 清除LOG檔 只保留一週
        $this->cleanLogs($date);

        // 清除 attention_stocks 只保留一個月
        $this->cleanAttentionStocks($date);

        // 清除 compare_daily_stocks 只保留一個月
        $this->cleanCompareDailyStocks($date);

        // 清除 five_minutes_stocks 只保留一個月
        $this->cleanFiveMinutesStocks($date);

        // 清除 like_stocks 只保留一個月
        $this->cleanLikeStocks($date);

        // 清除 rise_up_stocks 只保留一個月
        $this->cleanRiseUpStocks($date);

        // 清除 simulation_stocks 只保留一年
        $this->cleanSimulationStocks($date);

        // 清除 stocks 只保留兩個月
        $this->cleanStocks($date);

        return 0;
    }

    /**
     * 清除 failed_jobs
     */
    private function cleanFailJobs()
    {
        DB::table('failed_jobs')->truncate();

        $this->line('清除failed_jobs 完畢');
    }

    /**
     * 清除LOG檔 只保留一週
     *
     * @param string $date
     */
    private function cleanLogs(string $date)
    {
        $before = Carbon::parse($date)->subDays(config('logging.channels.daily.days'));

        $files = glob(storage_path('/logs/laravel-*.log'));
        foreach ($files as $file) {
            $fileName = basename($file, '.log');
            $fileDate = Carbon::parse(str_replace('laravel-', '', $fileName));

            // 7天內保留
            if ($fileDate->greaterThan($before)) {
                continue;
            }

            // 其餘就清除
            unlink($file);
        }

        $this->line('清除Logs 完畢');
    }

    /**
     * 清除 attention_stocks 只保留一個月
     *
     * @param string $date
     */
    private function cleanAttentionStocks(string $date)
    {
        $before = Carbon::parse($date)->subMonths(1);

        $this->attentionStockRepository->deleteByWhere(
            [
                ['created_at', '<', $before]
            ]
        );

        $this->line('清除 attention_stocks 完畢');
    }

    /**
     * 清除 compare_daily_stocks 只保留一個月
     *
     * @param string $date
     */
    private function cleanCompareDailyStocks(string $date)
    {
        $before = Carbon::parse($date)->subMonths(1);

        $this->compareDailyStocksRepository->deleteByWhere(
            [
                ['created_at', '<', $before]
            ]
        );

        $this->line('清除 compare_daily_stocks 完畢');
    }

    /**
     * 清除 five_minutes_stocks 只保留一個月
     *
     * @param string $date
     */
    private function cleanFiveMinutesStocks(string $date)
    {
        $before = Carbon::parse($date)->subMonths(1);

        $this->fiveMinutesStocksRepository->deleteByWhere(
            [
                ['created_at', '<', $before]
            ]
        );

        $this->line('清除 five_minutes_stocks 完畢');
    }

    /**
     * 清除 like_stocks 只保留一個月
     *
     * @param string $date
     */
    private function cleanLikeStocks(string $date)
    {
        $before = Carbon::parse($date)->subMonths(1);

        $this->likeStockRepository->deleteByWhere(
            [
                ['created_at', '<', $before]
            ]
        );

        $this->line('清除 like_stocks 完畢');
    }

    /**
     * 清除 rise_up_stocks 只保留一個月
     *
     * @param string $date
     */
    private function cleanRiseUpStocks(string $date)
    {
        $before = Carbon::parse($date)->subMonths(1);

        $this->riseUpStockRepository->deleteByWhere(
            [
                ['created_at', '<', $before]
            ]
        );

        $this->line('清除 rise_up_stocks 完畢');
    }

    /**
     * 清除 simulation_stocks 只保留一年
     *
     * @param string $date
     * @throws \Exception
     */
    private function cleanSimulationStocks(string $date)
    {
        $before = Carbon::parse($date)->subYears(1);

        $this->simulationStockBoughtRepository->deleteByWhere(
            [
                ['created_at', '<', $before]
            ]
        );

        $this->simulationStockSoldRepository->deleteByWhere(
            [
                ['created_at', '<', $before]
            ]
        );

        $this->line('清除 simulation_stocks 完畢');
    }

    /**
     * 清除 stocks 只保留兩個月
     *
     * @param string $date
     */
    private function cleanStocks(string $date)
    {
        $before = Carbon::parse($date)->subMonths(2);

        $this->stockRepository->deleteByWhere(
            [
                ['created_at', '<', $before]
            ]
        );

        $this->line('清除 stocks 完畢');
    }
}
