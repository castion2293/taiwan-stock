<?php

namespace App\Console\Commands;

use App\Services\RiseUpStockService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class RiseUpCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rise-up:stock {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '計算連續上漲';

    /**
     * @var RiseUpStockService
     */
    protected $riseUpStockService;

    /**
     * Create a new command instance.
     *
     * @param RiseUpStockService $riseUpStockService
     */
    public function __construct(RiseUpStockService $riseUpStockService)
    {
        parent::__construct();
        $this->riseUpStockService = $riseUpStockService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        $endDate = $this->option('date');
        if (empty($endDate)) {
            $endDate = today()->toDateString();
        }

        $startDate = Carbon::parse($endDate)->subMonths(1)->toDateString();

        $allStockNos = array_keys(platform('stock_code'));
        $bar = $this->output->createProgressBar(count($allStockNos));
        $bar->start();

        $allStockNoChunks = collect($allStockNos)->chunk(50)->toArray();
        foreach ($allStockNoChunks as $allStockNoChunk) {
            $result = $this->riseUpStockService->calculateRiseUpStocks($allStockNoChunk, $startDate, $endDate);

            if (Arr::get($result, 'code') !== platform('api_code.success')) {
                $this->error(Arr::get($result, 'error'));
            }

            $bar->advance(50);
        }

        $bar->finish();

        $this->info('計算完畢');
        return 0;
    }
}
