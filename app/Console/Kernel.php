<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // 每工作日 09:00~13:30 爬蟲更新資訊
        $schedule->command('fetch:stock')
            ->everyThreeMinutes()
            ->weekdays()
            ->between('09:00', '13:40');

        // 比較前一日成交量
        $schedule->command('compare:daily-stocks')
            ->everyTenMinutes()
            ->weekdays()
            ->between('09:00', '13:40')
            ->runInBackground();

        // 每日計算連續上漲及紅K天數
        $schedule->command('rise-up:stock')
            ->dailyAt('13:45')
            ->weekdays()
            ->runInBackground();

        // 每日抓取均線黃金交叉股票
        $schedule->command('fetch:golden-cross')
            ->dailyAt('23:00')
            ->weekdays()
            ->runInBackground();

        // 每日抓取均線死亡交叉股票
        $schedule->command('fetch:death-cross')
            ->dailyAt('23:00')
            ->weekdays()
            ->runInBackground();

        // 每日抓取均線方向改變股票
        $schedule->command('fetch:direction-turn')
            ->dailyAt('23:10')
            ->weekdays()
            ->runInBackground();

        // 每月更新一次股票股利資訊
        $schedule->command('fetch:dividend')
            ->monthly()
            ->runInBackground();

        // 每天固定清楚任務
        $schedule->command('daily:clean')
            ->daily()
            ->runInBackground();

        // 每5分鐘檢查關注股票 改成每次拉新股票時檢查
//        $schedule->command('check:attention')
//            ->everyFiveMinutes()
//            ->weekdays()
//            ->between('09:00', '13:40')
//            ->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
