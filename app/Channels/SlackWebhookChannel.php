<?php

namespace App\Channels;

use Illuminate\Notifications\Channels\SlackWebhookChannel as LaravelSlackWebhookChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Str;

class SlackWebhookChannel extends LaravelSlackWebhookChannel
{
    public function sendMessage(Notification $notification)
    {

        // 檢查 ENV Slack 總開關
        if (!config('platform.slack_enable')) {
            return false;
        }

        // 檢查個別 notification 開關
        $switchName = Str::of(class_basename($notification))->snake()->append('_enable');
        if (!config("slack_webhook.{$switchName}")) {
            return false;
        }

        $webHookUrl = config('platform.slack_webhook_url');

        if (empty($webHookUrl)) {
            throw new \Exception('Slack Webhook URL 還沒有設定');
        }

        return $this->http->post($webHookUrl, $this->buildJsonPayload(
            $notification->toSlack()
        ));
    }
}
