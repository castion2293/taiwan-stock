<?php

namespace App\ApiCallers;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class DividendApiCaller
{
    /**
     * 呼叫API
     *
     * @param string $stockNo
     * @return array
     * @throws \Exception
     */
    public function callApi(string $stockNo)
    {
        $dividendApiUrl = config('platform.dividend_api_url');

        $response = Http::timeout(5)->get("{$dividendApiUrl}%3A{$stockNo}%3ASTOCK/divided");
        $data = $response->json();

        // message !== OK 拋例外
        $message = Arr::get($data, 'message');
        if ($message !== 'OK') {
            throw new \Exception($message);
        }

        $dividends = Arr::get($data, 'data.divides');
        if (empty($dividends)) {
            throw new \Exception("{$stockNo}: 沒有股利資訊");
        }

        return collect($dividends)->map(
            function ($dividend) use ($stockNo) {
                return [
                    'stock_no' => Str::wrap($stockNo, '\''),
                    'stock_name' => Str::wrap(platform("stock_code.{$stockNo}"), '\''),
                    'divided_date' => Str::wrap(Carbon::parse($dividend['formatDate'])->toDateString(), '\''),
                    'dividing_price' => (!empty($dividend['formatPreClose'])) ? floatval($dividend['formatPreClose']) : 0,
                    'dividend' => (!empty($dividend['formatCashDividend'])) ? floatval($dividend['formatCashDividend']) : 0,
                    'dividend_percent' => (!empty($dividend['formatDividendYield'])) ? floatval($dividend['formatDividendYield']) : 0,
                    'stock_dividend' => (!empty($dividend['formatStockDividend'])) ? floatval($dividend['formatStockDividend']) : 0,
                    'free_dividend' => floatval($dividend['formatFreeDividend']),
                    'issuance_stock_dividend' => floatval($dividend['formatIssuanceStockDividend']),
                    'call_price' => floatval($dividend['formatCallPrice']),
                ];
            }
        )->toArray();
    }
}
