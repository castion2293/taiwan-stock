<?php

use Carbon\Carbon;

if (! function_exists('platform')) {
    /**
     * Get the platform file.
     *
     * @param string $file
     * @return string
     * @throws Exception
     */
    function platform(string $file)
    {
        $fileArray = explode('.', $file);

        $fileName = array_shift($fileArray);
        $filePathName = resource_path() . '/platform/' . $fileName . '.php';

        if (!file_exists($filePathName)) {
            throw new \Exception($filePathName . ' 檔案不存在');
        }

        $fileData = include $filePathName;

        // 沒有指定key 直接回傳整個陣列資料
        if (empty($fileArray)) {
            return $fileData;
        }

        return  Arr::get($fileData, implode('.', $fileArray));
    }
}

if (! function_exists('get_date')) {
    /**
     * 定義交易日期
     *
     * @param string $timeLine
     * @return string
     */
    function get_date(string $timeLine = '09:00:00'): string
    {
        if (now()->greaterThan(Carbon::parse($timeLine))) {
            // 開始交易後
            $date = now();
        } else {
            // 開始交易前
            $date = now()->subDays(1);
        }

        // 遇到假日 改成最近的星期五
        if ($date->isWeekend()) {
            $date = now()->previousWeekday();
        }

        return $date->toDateString();
    }
}

if (! function_exists('put_string_mark')) {
    /**
     * 在字串列前後補上引號
     *
     * @param string $string
     * @return string
     */
    function put_string_mark(string $string)
    {
        return '\'' . $string . '\'';
    }
}

if (! function_exists('get_stock_name')) {
    /**
     * 帶入股票代號取得股票名稱
     *
     * @param string $stockNo
     * @return string
     * @throws Exception
     */
    function get_stock_name(string $stockNo)
    {
        return Arr::get(platform('stock_code'), $stockNo, '');
    }
}
