<?php

namespace App\Crawlers;

use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;

abstract class AbstractCrawler
{
    /**
     * 爬蟲抓取器
     * @var Client
     */
    protected $client;

    public function __construct()
    {
        $this->client = new Client(HttpClient::create(['timeout' => 10]));
    }

    /**
     * 爬蟲抓取
     *
     * @param string $stockNo
     */
    abstract public function crawler(string $stockNo);
}
