<?php

namespace App\Crawlers;

class DeathCrossCrawler extends AbstractCrawler
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 爬蟲抓取
     *
     * @param string $stockNo
     * @return array
     */
    public function crawler(string $stockNo = ''): array
    {
        $deathCrossStocks = [];
        $deathCrossUrl = config('platform.death_cross_url');

        $crawler = $this->client->request('GET', $deathCrossUrl);

        // 日期
        $deathCrossStocks['date'] = $crawler->filter('.tydate')->text();

        // 股票代號
        $deathCrossStocks['stock_no'] = [];
        $crawler->filter('table')
            ->first()
            ->filter('tr > td > a')
            ->each(function ($node, $i) use (&$deathCrossStocks) {
                if ($i % 2 === 0) {
                    array_push($deathCrossStocks['stock_no'], $node->text());
                }
            });

        return $deathCrossStocks;
    }
}
