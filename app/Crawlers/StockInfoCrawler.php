<?php

namespace App\Crawlers;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler;

class StockInfoCrawler extends AbstractCrawler
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 爬蟲抓取
     *
     * @param string $stockNo
     * @return array
     */
    public function crawler(string $stockNo)
    {
        $stock = [];
        $stockUrl = config('platform.taiwan_stock_url');

        $crawler = $this->client->request('GET', "{$stockUrl}{$stockNo}/overview");

        // 股票代碼
        $stock['stock_no'] = $stockNo;

        // 股票名稱
        $stock['stock_name'] = get_stock_name($stockNo);

        // 日期
        $headerTime = $crawler->filter('.header-time > time')->text();

        $stock['date'] = Carbon::parse($headerTime)->addHours(8)->toDateString();

        $nodeData = [];
        $crawler->filter('.block-value')->each(
            function (Crawler $node) use (&$nodeData) {
                array_push($nodeData, $node->text());
            }
        );

        // 成交張數
        $stocksString = Arr::get($nodeData, 0);

        $stocks = intval($stocksString);

        if (Str::of($stocksString)->contains('K')) {
            $stocks = intval(floatval(str_replace('K', '', $stocksString)) * 1000);
        }

        if (Str::of($stocksString)->contains('M')) {
            $stocks = intval(floatval(str_replace('M', '', $stocksString)) * 1000);
        }

        $stock['stocks'] = $stocks;

        // 開盤價
        $stock['start_price'] = floatval(str_replace(',', '', Arr::get($nodeData, 4)));

        // 收盤價
        $stock['end_price'] = floatval(str_replace(',', '', Arr::get($nodeData, 5)));

        // 昨天收盤價
        $stock['last_end_price'] = floatval(str_replace(',', '', Arr::get($nodeData, 3)));

        // 漲跌價差
        $stock['price_diff'] = floatval($crawler->filter('.change-net > span')->text());

        // 漲跌價差百分比
        $diffPercentString = $crawler->filter('.change-percent > span')->text();
        $stock['price_diff_percent'] = floatval(str_replace('%', '', $diffPercentString));

        return $stock;
    }
}
