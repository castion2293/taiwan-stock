<?php

namespace App\Crawlers;

use Illuminate\Support\Arr;

class DirectionTurnCrawler extends AbstractCrawler
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 爬蟲抓取
     *
     * @param string $stockNo
     * @return array
     */
    public function crawler(string $stockNo = '')
    {
        $directionTurnStocks = [];
        $directionTurnUrl = config('platform.direction_turn_url');

        $crawler = $this->client->request('GET', $directionTurnUrl);

        // 日期
        $directionTurnStocks['date'] = $crawler->filter('.tydate')->text();
        $directionTurnStocks = $this->getDirectionTurnStocks($directionTurnStocks, $crawler, 0, 1);

        // 爬蟲取不到第一個table 就往下一個 table 找
        if (empty(Arr::get($directionTurnStocks, 'turn_up_stock_no'))) {
            $directionTurnStocks = $this->getDirectionTurnStocks($directionTurnStocks, $crawler, 1, 2);
        }

        return $directionTurnStocks;
    }

    /**
     * @param array $directionTurnStocks
     * @param \Symfony\Component\DomCrawler\Crawler|null $crawler
     * @param int $turnUpPosition
     * @param int $turnDownPosition
     * @return array
     */
    private function getDirectionTurnStocks(
        array $directionTurnStocks,
        ?\Symfony\Component\DomCrawler\Crawler $crawler,
        int $turnUpPosition,
        int $turnDownPosition
    ): array {
        // 5日均線反轉上揚個股
        $directionTurnStocks['turn_up_stock_no'] = [];
        $crawler->filter('table')
            ->eq($turnUpPosition)
            ->filter('tr > td > a')
            ->each(
                function ($node, $i) use (&$directionTurnStocks) {
                    if ($i % 2 === 0) {
                        array_push($directionTurnStocks['turn_up_stock_no'], $node->text());
                    }
                }
            );

        // 5日均線反轉下彎個股
        $directionTurnStocks['turn_down_stock_no'] = [];
        $crawler->filter('table')
            ->eq($turnDownPosition)
            ->filter('tr > td > a')
            ->each(
                function ($node, $i) use (&$directionTurnStocks) {
                    if ($i % 2 === 0) {
                        array_push($directionTurnStocks['turn_down_stock_no'], $node->text());
                    }
                }
            );

        return $directionTurnStocks;
    }
}
