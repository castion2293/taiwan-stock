<?php

namespace App\Crawlers;

use Illuminate\Support\Arr;

class GoldenCrossCrawler extends AbstractCrawler
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 爬蟲抓取
     *
     * @param string $stockNo
     * @return array
     */
    public function crawler(string $stockNo = ''): array
    {
        $goldenCrossStocks = [];
        $goldenCrossUrl = config('platform.golden_cross_url');

        $crawler = $this->client->request('GET', $goldenCrossUrl);

        // 日期
        $goldenCrossStocks['date'] = $crawler->filter('.tydate')->text();

        // 股票代號
        $goldenCrossStocks['stock_no'] = [];
        $goldenCrossStocks = $this->getGoldenCrossStocks($crawler, $goldenCrossStocks, 0);

        // 爬蟲取不到第一個table 就往下一個 table 找
        if (empty(Arr::get($goldenCrossStocks, 'stock_no'))) {
            $goldenCrossStocks = $this->getGoldenCrossStocks($crawler, $goldenCrossStocks, 1);
        }

        return $goldenCrossStocks;
    }

    /**
     * @param \Symfony\Component\DomCrawler\Crawler|null $crawler
     * @param array $goldenCrossStocks
     * @param int $position
     * @return array
     */
    private function getGoldenCrossStocks(
        ?\Symfony\Component\DomCrawler\Crawler $crawler,
        array $goldenCrossStocks,
        int $position
    ): array {
        $crawler->filter('table')
            ->eq($position)
            ->filter('tr > td > a')
            ->each(
                function ($node, $i) use (&$goldenCrossStocks) {
                    if ($i % 2 === 0) {
                        array_push($goldenCrossStocks['stock_no'], $node->text());
                    }
                }
            );
        return $goldenCrossStocks;
    }
}
