<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Dividend extends Component
{
    use WithSorting;

    public $viewData;

    public function mount()
    {
        // 排序時 需保留的欄位名稱
        $this->keepColumns = [
            'stock_no',
            'year'
        ];
    }

    public function render()
    {
        return view('livewire.dividend');
    }
}
