<?php

namespace App\Http\Livewire;

use App\Services\SettingService;
use Illuminate\Support\Arr;
use Livewire\Component;

class Setting extends Component
{
    public $viewData;

    public function render()
    {
        return view('livewire.setting');
    }

    /**
     * 修改設定
     *
     * @param $key
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function updateSetting(string $key)
    {
        $settingService = \App::make(SettingService::class);
        $result = $settingService->updateSetting($key, Arr::get($this->viewData, $key));

        if (Arr::get($result, 'code') !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        session()->flash('message', '更新成功');

        return redirect()->to('setting');
    }
}
