<?php

namespace App\Http\Livewire;

use Livewire\Component;

class LikeStocks extends Component
{
    use WithSorting;

    public $viewData;

    public function render()
    {
        return view('livewire.like-stocks');
    }
}
