<?php

namespace App\Http\Livewire;

use Illuminate\Support\Arr;

trait WithSorting
{
    /**
     * 路由名稱
     *
     * @var
     */
    public $url;

    /**
     * 路由參數
     *
     * @var
     */
    public $params;

    /**
     * 排序時 需保留的欄位名稱
     *
     * @var array
     */
    public $keepColumns = [];

    /**
     * 欄位排序
     *
     * @param string $columnSort
     * @param array $keepColumns
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sort(string $columnSort)
    {
        $columnSortType = Arr::get($this->params, $columnSort);

        if ($columnSortType === null || $columnSortType === 'asc') {
            $columnSortType = 'desc';
        } else {
            $columnSortType = 'asc';
        }

        $fullUrl = $this->url . '?' . http_build_query(
            array_merge(
                Arr::only($this->params, $this->keepColumns),
                [
                        $columnSort => $columnSortType
                    ]
            )
        );

        return redirect()->to($fullUrl);
    }
}
