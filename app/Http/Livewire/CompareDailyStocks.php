<?php

namespace App\Http\Livewire;

use Illuminate\Support\Arr;
use Livewire\Component;

class CompareDailyStocks extends Component
{
    use WithSorting;

    public $viewData;

    public function mount()
    {
        // 排序時 需保留的欄位名稱
        $this->keepColumns = ['date'];
    }

    public function render()
    {
        return view('livewire.compare-daily-stocks');
    }
}
