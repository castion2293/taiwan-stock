<?php

namespace App\Http\Livewire;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Livewire\Component;

class Filter extends Component
{
    /**
     * 路由名稱
     *
     * @var
     */
    public $url;

    /**
     * 路由參數
     *
     * @var
     */
    public $params;

    /**
     * 可以過慮的條件
     *
     * @var
     */
    public $models;

    /**
     * 過濾條件參數
     *
     * @var
     */
    public $filterParams;

    public function mount()
    {
        foreach ($this->models as $model) {
            // 檢查是否是 active 狀態 false => continue
            if (!Arr::get($model, 'modelActive', true)) {
                continue;
            }

            $code = Arr::get($model, 'modelCode');
            $label = Arr::get($model, 'modelLabel');
            $this->filterParams[$code] = [
                'label' => $label,
                'model' => request()->input(Str::snake($code), '')
            ];
        }
    }

    public function render()
    {
        return view('livewire.filter');
    }

    /**
     * 送出篩選條件
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function submitUrl()
    {
        $filters = collect($this->filterParams)->mapWithKeys(
            function ($filterParam, $code) {
                return [
                    Str::snake($code) => Arr::get($filterParam, 'model')
                ];
            }
        )
            ->reject(
                function ($filterValue) {
                    return empty($filterValue);
                }
            )
            ->toArray();

        $fullUrl = $this->url . '?' . http_build_query(
            array_merge(
                Arr::only($this->params, ['status']),
                $filters
            )
        );

        return redirect()->to($fullUrl);
    }

    /**
     * 重置篩選條件
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resetUrl()
    {
        return redirect()->to($this->url);
    }
}
