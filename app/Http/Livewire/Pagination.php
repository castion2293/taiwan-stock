<?php

namespace App\Http\Livewire;

use Illuminate\Support\Arr;
use Livewire\Component;

class Pagination extends Component
{
    public $viewData;

    public function render()
    {
        $path = Arr::get($this->viewData, 'path');
        $currentPage = Arr::get($this->viewData, 'current_page');

        // 上一頁
        $prevPage = (($currentPage - 1) <= 0) ? $currentPage : $currentPage - 1;
        $prevPath = $path . '/?' . $this->getRequestQuery($prevPage);

        // 下一頁
        $nextPage = (($currentPage + 1) > Arr::get($this->viewData, 'last_page'))
            ? Arr::get($this->viewData, 'last_page')
            : $currentPage + 1;
        $nextPath = $path . '?' . $this->getRequestQuery($nextPage);

        // 第一頁
        $firstPath = $path . '?' . $this->getRequestQuery(1);

        // 最後頁
        $lastPage = Arr::get($this->viewData, 'last_page');
        $lastPath = $path . '?' . $this->getRequestQuery($lastPage);

        return view(
            'livewire.pagination',
            [
                'prevPath' => $prevPath,
                'nextPath' => $nextPath,
                'firstPath' => $firstPath,
                'lastPath' => $lastPath

            ]
        );
    }

    /**
     * 獲取完整的url query
     *
     * @param $page
     * @return string
     */
    private function getRequestQuery($page)
    {
        $requestData = array_merge(['page' => (string)$page], request()->except('page'));
        return http_build_query($requestData);
    }
}
