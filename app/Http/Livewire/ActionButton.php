<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Jetstream\Contracts\DeletesUsers;
use Livewire\Component;

class ActionButton extends Component
{
    public $data;

    /**
     * 執行動作
     *
     * @var
     */
    public $action;

    /**
     * 路由名稱
     *
     * @var
     */
    public $url;

    /**
     * 路由參數
     *
     * @var
     */
    public $params;

    public function render()
    {
        return view('livewire.action-button');
    }

    /**
     * 開啟 Modal
     */
    public function popModal()
    {
        $this->emitTo(
            'modal',
            'openModal',
            [
                'view_data' => $this->data,
                'action' => $this->action,
                'url' => $this->url,
                'params' => $this->params
            ]
        );
    }
}
