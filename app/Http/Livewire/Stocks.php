<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Stocks extends Component
{
    use WithSorting;

    public $viewData;

    public function render()
    {
        return view('livewire.stocks');
    }
}
