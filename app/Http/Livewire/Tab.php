<?php

namespace App\Http\Livewire;

use Illuminate\Support\Arr;
use Livewire\Component;

class Tab extends Component
{
    /**
     * 路由名稱
     *
     * @var
     */
    public $url;

    /**
     * 路由參數
     *
     * @var
     */
    public $params;

    /**
     * tab model
     *
     * @var
     */
    public $navs;

    public function mount()
    {
        $this->navs = collect($this->navs)->map(
            function ($nav) {
                $nav['url'] = $this->url . '?' . http_build_query(
                    array_merge(
                        Arr::only($this->params, $nav['keep_filter']),
                        [
                                'status' => Arr::get($nav, 'code')
                            ]
                    )
                );

                return $nav;
            }
        )->toArray();
    }

    public function render()
    {
        return view('livewire.tab');
    }
}
