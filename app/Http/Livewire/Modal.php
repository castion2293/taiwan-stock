<?php

namespace App\Http\Livewire;

use App\Repositories\StockRepository;
use App\Services\AttentionStockService;
use App\Services\LikeStockService;
use App\Services\SimulationStockService;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Livewire\Component;
use RealRashid\SweetAlert\Facades\Alert;

class Modal extends Component
{
    /**
     * Indicates if user deletion is being confirmed.
     *
     * @var bool
     */
    public $open = false;

    /**
     *
     * @var bool
     */
    public $hidden = true;

    /**
     * 目前價格
     *
     * @var float
     */
    public $currentPrice = 0;

    /**
     * 購買張數
     *
     * @var int
     */
    public $buyStocks = 0;

    /**
     * 販賣張數
     *
     * @var int
     */
    public $sellStocks = 0;

    public $viewData = [];

    /**
     * 執行動作
     *
     * @var
     */
    public $action;

    /**
     * 路由名稱
     *
     * @var
     */
    public $url = '';

    /**
     * 路由參數
     *
     * @var
     */
    public $params = [];

    protected $listeners = ['openModal'];

    public function render()
    {
        return view('livewire.modal');
    }

    /**
     * 打開 Modal
     *
     * @param array $data
     */
    public function openModal(array $data)
    {
        $this->viewData = Arr::get($data, 'view_data');
        $this->action = Arr::get($data, 'action');
        $this->url = Arr::get($data, 'url');
        $this->params = Arr::get($data, 'params');

        $this->currentPrice = 0;
        $this->buyStocks = 1;
        $this->sellStocks = 1;
        $this->hidden = true;
        $this->open = true;
    }

    /*
     * 執行動作
     */
    public function doAction()
    {
        $actionName = Str::camel($this->action);
        $this->$actionName();

        return redirect()->to($this->url . '?' . http_build_query($this->params));
    }

    /**
     * 取得目前的股價
     */
    public function getCurrentPrice()
    {
        $stockNo = Arr::get($this->viewData, 'stock_no');
        $attentionStockService = \App::make(AttentionStockService::class);

        $result = $attentionStockService->getLastEndPrice($stockNo, now()->toDateString());

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        $this->currentPrice = Arr::get($result, 'data.price');

        // 顯示目前價格
        $this->hidden = false;
    }

    /**
     * 模擬買股票
     *
     * @return void
     * @throws \Exception
     */
    private function buySimulationStock()
    {
        $stockNo = Arr::get($this->viewData, 'stock_no');

        // 驗證會員餘額足夠買此張股票
        $userBalance = auth()->user()->balance;

        $stockRepository = \App::make(StockRepository::class);
        $endPrice = $stockRepository->getLastEndPrice($stockNo, now()->toDateString());

        if ($userBalance < $amount = $endPrice * 1000 * $this->buyStocks) {
            Alert::error('模擬買股票', "額度不足 目前餘額 : \${$userBalance} 小於 付出成本: \${$amount}");
            return;
        }

        $simulationStockService = \App::make(SimulationStockService::class);

        $result = $simulationStockService->buySimulationStock(
            [
                'user_id' => auth()->user()->id,
                'stock_no' => $stockNo,
                'buy_stocks' => $this->buyStocks,
                'amount' => $amount
            ]
        );

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        Alert::success(get_stock_name($stockNo) . '(' . $stockNo . ')模擬買入' . $this->buyStocks . '張', '成功');
    }

    /**
     * 模擬賣股票
     */
    private function sellSimulationStock()
    {
        // 檢查販賣張數是否足夠
        if ($this->sellStocks > $this->viewData['buy_stocks']) {
            Alert::error('模擬賣股票', "販賣張數: {$this->sellStocks}張 大於 購買張數: {$this->viewData['buy_stocks']}張");
            return;
        }

        $simulationStockService = \App::make(SimulationStockService::class);

        $result = $simulationStockService->sellSimulationStock(
            [
                'simulation_stock_bought_id' => Arr::get($this->viewData, 'id'),
                'sale_stocks' => $this->sellStocks,
            ]
        );

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        Alert::success('模擬賣股票' . $this->sellStocks . '張', '成功');
    }

    /**
     * 新增觀注股票
     */
    private function addAttentionStock()
    {
        $attentionStockService = \App::make(AttentionStockService::class);

        $result = $attentionStockService->store(
            [
                'user_id' => auth()->user()->id,
                'stock_no' => $stockNo = Arr::get($this->viewData, 'stock_no'),
                'target_price' => floatval(Arr::get($this->viewData, 'target_price')),
                'greater' => (Arr::get($this->viewData, 'greater') === (string)platform(
                    'status_code.attention_stocks.greater'
                )),
            ]
        );

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        Alert::success(get_stock_name($stockNo) . '(' . $stockNo . ')加入觀注股票', '成功');
    }

    /**
     * 停止觀注股票
     */
    private function stopAttentionStock()
    {
        $attentionStockService = \App::make(AttentionStockService::class);

        $result = $attentionStockService->updateStatus(
            [Arr::get($this->viewData, 'id')],
            platform('status_code.attention_stocks.not_attend')
        );

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        Alert::success('停止觀注', '成功');
    }

    /**
     * 加入欲買清單
     */
    private function likeStock()
    {
        $likeStockService = \App::make(LikeStockService::class);

        $result = $likeStockService->addLikeStock(
            [
                'user_id' => auth()->user()->id,
                'stock_no' => $stockNo = Arr::get($this->viewData, 'stock_no'),
            ]
        );

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        Alert::success(get_stock_name($stockNo) . '(' . $stockNo . ')加入欲買股票', '成功');
    }

    /**
     * 移除欲買清單
     */
    private function removeLikeStock()
    {
        $likeStockService = \App::make(LikeStockService::class);

        $id = Arr::get($this->viewData, 'id');
        $result = $likeStockService->removeLikeStock($id);

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        Alert::success('移除欲買股票', '成功');
    }

    /**
     * 重置模擬買入股票均線狀態
     */
    private function resetSimulationStock()
    {
        $simulationStockService = \App::make(SimulationStockService::class);

        $result = $simulationStockService->resetSimulationStock(
            [
                'id' => Arr::get($this->viewData, 'id')
            ]
        );

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        Alert::success('重置模擬買入股票均線狀態', '成功');
    }

    /**
     * 重置欲買股票均線狀態
     */
    private function resetLikeStock()
    {
        $likeStockService = \App::make(LikeStockService::class);

        $result = $likeStockService->resetLikeStock(
            [
                'id' => Arr::get($this->viewData, 'id')
            ]
        );

        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'));
        }

        Alert::success('重置欲買股票均線狀態', '成功');
    }
}
