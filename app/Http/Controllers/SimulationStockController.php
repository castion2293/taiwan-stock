<?php

namespace App\Http\Controllers;

use App\Services\SimulationStockService;
use Illuminate\Http\Request;

class SimulationStockController extends Controller
{
    /**
     * @var SimulationStockService
     */
    protected $simulationStockService;

    public function __construct(SimulationStockService $simulationStockService)
    {
        $this->simulationStockService = $simulationStockService;
    }

    /**
     * 模擬買賣
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if (empty($request->all())) {
            $routeParams = [
                'status' => platform('status_code.simulation_stocks.buy'),
                'buy_date_sort' => 'desc'
            ];

            return redirect(route('simulation_stocks', $routeParams));
        }

        // 選擇 "庫存損益" 標籤
        $isListForBought = ($request->input('status') === (string)platform('status_code.simulation_stocks.buy') && empty($request->except('status')));
        if ($isListForBought) {
            $routeParams = [
                'status' => $request->input('status'),
                'buy_date_sort' => 'desc'
            ];

            return redirect(route('simulation_stocks', $routeParams));
        }

        // 選擇 "已實損益" 標籤
        $isListForSold = ($request->input('status') === (string)platform('status_code.simulation_stocks.sell') && empty($request->except('status')));
        if ($isListForSold) {
            $routeParams = [
                'status' => $request->input('status'),
                'sale_date_sort' => 'desc'
            ];

            return redirect(route('simulation_stocks', $routeParams));
        }

        $params = [
            'status' => $request->input('status'),
        ];

        $params = $params + $request->all();

        return $this->responseWithView('simulation_stocks', $this->simulationStockService->getList($params));
    }
}
