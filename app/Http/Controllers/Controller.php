<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Arr;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * 統整要回傳前端的資料格式
     *
     * @param string $viewName
     * @param array $result
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Exception
     */
    protected function responseWithView(string $viewName, array $result)
    {
        $code = Arr::get($result, 'code');
        if ($code !== platform('api_code.success')) {
            throw new \Exception(Arr::get($result, 'error'), $code);
        }

        return view($viewName, Arr::get($result, 'data'));
    }
}
