<?php

namespace App\Http\Controllers;

use App\Services\AttentionStockService;
use Illuminate\Http\Request;

class AttentionStockController extends Controller
{
    /**
     * @var AttentionStockService
     */
    protected $attentionStockService;

    public function __construct(AttentionStockService $attentionStockService)
    {
        $this->attentionStockService = $attentionStockService;
    }

    public function index(Request $request)
    {
        if (empty($request->all())) {
            $routeParams = [
                'status' => platform('status_code.attention_stocks.attending'),
            ];

            return redirect(route('attention_stocks', $routeParams));
        }

        $params = [
            'status' => $request->input('status'),
            'attended_at_sort' => $request->input('attended_at_sort') ?? 'desc'
        ];

        $params = $params + $request->all();

        return $this->responseWithView('attention_stocks', $this->attentionStockService->getList($params));
    }
}
