<?php

namespace App\Http\Controllers;

use App\Services\RiseUpStockService;
use Illuminate\Http\Request;

class RiseUpController extends Controller
{
    /**
     * @var RiseUpStockService
     */
    protected $riseUpStockService;

    public function __construct(RiseUpStockService $riseUpStockService)
    {
        $this->riseUpStockService = $riseUpStockService;
    }

    /**
     * 連續上漲
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if (empty($request->all())) {
            $routeParams = [
                'end_date' => get_date('13:50'),
                'recent_rise_up_days' => 3,
                'recent_red_k_days' => 2,
                'recent_rise_up_days_sort' => 'asc'
            ];

            return redirect(route('rise_up_stocks', $routeParams));
        }

        // 預設條件
        $params = [
            'end_date' => $request->input('end_date')
        ];

        $params = $params + $request->all();

        return $this->responseWithView('rise_up', $this->riseUpStockService->getList($params));
    }
}
