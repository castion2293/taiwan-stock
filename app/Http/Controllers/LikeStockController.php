<?php

namespace App\Http\Controllers;

use App\Services\LikeStockService;
use Illuminate\Http\Request;

class LikeStockController extends Controller
{
    /**
     * @var LikeStockService
     */
    protected $likeStockService;

    public function __construct(LikeStockService $likeStockService)
    {
        $this->likeStockService = $likeStockService;
    }

    /**
     * 欲買股票清單
     */
    public function index(Request $request)
    {
        if (empty($request->all())) {
            $routeParams = [
                'like_at_sort' => 'desc'
            ];

            return redirect(route('like_stocks', $routeParams));
        }

        $params = $request->all();

        return $this->responseWithView('like_stocks', $this->likeStockService->getList($params));
    }
}
