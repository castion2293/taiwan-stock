<?php

namespace App\Http\Controllers;

use App\Services\FiveMinutesStockService;
use Illuminate\Http\Request;

class FiveMinutesStockController extends Controller
{
    /**
     * @var FiveMinutesStockService
     */
    protected $fiveMinutesStockService;

    public function __construct(FiveMinutesStockService $fiveMinutesStockService)
    {
        $this->fiveMinutesStockService = $fiveMinutesStockService;
    }

    /**
     * 瞬間巨量
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if (empty($request->all())) {
            $routeParams = [
                'date' => get_date()
            ];

            return redirect(route('five_minutes_stocks', $routeParams));
        }

        $params = [
            'date' => $request->input('date')
        ];

        $params = $params + $request->all();

        return $this->responseWithView('five_minutes_stocks', $this->fiveMinutesStockService->getList($params));
    }
}
