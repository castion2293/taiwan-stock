<?php

namespace App\Http\Controllers;

use App\Services\CompareDailyStockService;
use Illuminate\Http\Request;

class CompareDailyStockController extends Controller
{
    /**
     * @var CompareDailyStockService
     */
    protected $compareDailyStockService;

    public function __construct(CompareDailyStockService $compareDailyStockService)
    {
        $this->compareDailyStockService = $compareDailyStockService;
    }

    /**
     * 成交倍增
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if (empty($request->all())) {
            $routeParams = [
                'date' => get_date()
            ];

            return redirect(route('compare_daily_stocks', $routeParams));
        }

        $params = [
            'date' => $request->input('date')
        ];

        $params = $params + $request->all();

        return $this->responseWithView('compare_daily_stocks', $this->compareDailyStockService->getList($params));
    }
}
