<?php

namespace App\Http\Controllers;

use App\Services\DividendService;
use Illuminate\Http\Request;

class DividendController extends Controller
{
    /**
     * @var DividendService
     */
    protected $dividendService;

    public function __construct(DividendService $dividendService)
    {
        $this->dividendService = $dividendService;
    }

    /**
     * 股票股利資訊
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $params = [
            'year_sort' => $request->input('year_sort') ?? 'desc'
        ];

        $params = $params + $request->all();

        return $this->responseWithView('dividend', $this->dividendService->getList($params));
    }
}
