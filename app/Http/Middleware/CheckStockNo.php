<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckStockNo
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle(Request $request, Closure $next)
    {
        // 如果輸入股票名稱 需轉換成代號
        $isStockName = ($request->has('stock_no')) && (!is_numeric($request->input('stock_no')));
        if ($isStockName) {
            $request = $this->changeStockNameToNumber($request, $request->input('stock_no'));
        }

        return $next($request);
    }

    /**
     * 股票名稱轉換成代號
     *
     * @param string $stockNo
     * @param Request $request
     * @return Request
     * @throws \Exception
     */
    private function changeStockNameToNumber(Request $request, string $stockNo): Request
    {
        $stockNo = array_search($stockNo, platform('stock_code'));

        // 找不到股票名稱 把 stock_no 從 request 移除掉
        if ($stockNo === false) {
            $request->request->remove('stock_no');
            return $request;
        }

        $request->merge(['stock_no' => $stockNo]);

        return $request;
    }
}
