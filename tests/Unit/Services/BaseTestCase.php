<?php

namespace Tests\Unit\Services;

use Tests\TestCase;
use Tests\SeederTestTrait;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BaseTestCase extends TestCase
{
    use SeederTestTrait;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }
}
