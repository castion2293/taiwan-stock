<?php

namespace Tests\Unit\Services;

use App\Services\StockService;
use Illuminate\Support\Arr;

class StockServiceTest extends BaseTestCase
{
    /**
     * @var StockService
     */
    protected $stockService;

    public function setUp(): void
    {
        parent::setUp();

        $this->stockService = $this->app->make(StockService::class);
    }

    /**
     * 測試 今日行情股市資訊
     *
     * @throws \Exception
     * @see \App\Services\StockService::getList()
     */
    public function testGetList()
    {
        // Arrange
        // 建立 股票種子資料
        $this->createStocks(get_date());

        $params = [
            'date' => get_date()
        ];

        // Act
        $result = $this->stockService->getList($params);

        // Assert
        $code = Arr::get($result, 'code');
        $data = Arr::get($result, 'data.viewData.data');

        $this->assertEquals(platform('api_code.success'), $code);

        $stock = Arr::first($data);
        $this->assertArrayHasKey('stock_no', $stock);
        $this->assertArrayHasKey('stock_name', $stock);
        $this->assertArrayHasKey('date', $stock);
        $this->assertArrayHasKey('end_price', $stock);
        $this->assertArrayHasKey('last_end_price', $stock);
        $this->assertArrayHasKey('price_diff', $stock);
        $this->assertArrayHasKey('price_diff_percent', $stock);
        $this->assertArrayHasKey('action', $stock);
        $this->assertArrayHasKey('like', $stock);
    }

    /**
     * 測試 今日行情股市資訊 已經模擬買入
     *
     * @see \App\Services\StockService::getList()
     * @throws \Exception
     */
    public function testGetListHasBought()
    {
        // Arrange
        // 建立 股票種子資料
        $stocks = $this->createStocks(get_date());
        $stockNos = Arr::pluck($stocks, 'stock_no');

        // 建立 模擬股票買賣 種子資料
        $this->createSimulationStocks($stockNos, platform('status_code.simulation_stocks.buy'));

        $params = [
            'date' => get_date()
        ];

        // Act
        $result = $this->stockService->getList($params);

        // Assert
        $code = Arr::get($result, 'code');
        $data = Arr::get($result, 'data.viewData.data');

        $this->assertEquals(platform('api_code.success'), $code);

        $stock = Arr::first($data);

        $this->assertEquals(platform('status_code.action.bought'), Arr::get($stock, 'action'));
    }

    /**
     * 測試 今日行情股市資訊 已經加入欲買
     *
     * @see \App\Services\StockService::getList()
     */
    public function testGetListHasLiked()
    {
        // Arrange
        // 建立 股票種子資料
        $stocks = $this->createStocks(get_date());
        $stockNos = Arr::pluck($stocks, 'stock_no');

        // 建立 欲買股票 種子資料
        $this->createLikeStocks($stockNos, platform('status_code.like_stocks.add_like'));

        $params = [
            'date' => get_date()
        ];


        // Act
        $result = $this->stockService->getList($params);

        // Assert
        $code = Arr::get($result, 'code');
        $data = Arr::get($result, 'data.viewData.data');

        $this->assertEquals(platform('api_code.success'), $code);

        $stock = Arr::first($data);

        $this->assertEquals(platform('status_code.action.removed_like_stock'), Arr::get($stock, 'like'));
    }
}
