<?php

namespace Tests;

use App\Models\LikeStock;
use App\Models\SimulationStocks;
use App\Models\Stock;

trait SeederTestTrait
{
    /**
     * 建立 股票種子資料
     *
     * @param string $date
     * @return array
     * @throws \Exception
     */
    protected function createStocks(string $date): array
    {
        $allStockCodes = platform('stock_code');
        $stockCodes = array_rand($allStockCodes, 25);

        $stocks = [];
        foreach ($stockCodes as $stockCode) {
            $stocks[] = Stock::factory()->create(
                [
                    'stock_no' => $stockCode,
                    'stock_name' => get_stock_name($stockCode),
                    'date' => $date
                ]
            )->toArray();
        }

        return $stocks;
    }

    /**
     * 建立 模擬股票買賣 種子資料
     *
     * @param array $stockNos
     * @param int $status
     * @return array
     * @throws \Exception
     */
    protected function createSimulationStocks(array $stockNos = [], int $status = 0): array
    {
        $simulationStocks = [];
        foreach ($stockNos as $stockNo) {
            $simulationStocks[] = SimulationStocks::factory()->create(
                [
                    'stock_no' => $stockNo,
                    'stock_name' => get_stock_name($stockNo),
                    'status' => $status
                ]
            )->toArray();
        }

        return $simulationStocks;
    }

    /**
     * 建立 欲買股票 種子資料
     *
     * @param array $stockNos
     * @param int $status
     * @return array
     * @throws \Exception
     */
    protected function createLikeStocks(array $stockNos = [], int $status = 0): array
    {
        $likeStocks = [];
        foreach ($stockNos as $stockNo) {
            $likeStocks[] = LikeStock::factory()->create(
                [
                    'stock_no' => $stockNo,
                    'stock_name' => get_stock_name($stockNo),
                    'status' => $status
                ]
            )->toArray();
        }

        return $likeStocks;
    }
}
