<?php

use App\Http\Controllers\AttentionStockController;
use App\Http\Controllers\CompareDailyStockController;
use App\Http\Controllers\DividendController;
use App\Http\Controllers\FiveMinutesStockController;
use App\Http\Controllers\LikeStockController;
use App\Http\Controllers\RiseUpController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SimulationStockController;
use App\Http\Controllers\StockController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/stocks');

Route::middleware(['auth:sanctum', 'verified', 'check.stockNo'])->group(function () {
    // 今日行情
    Route::get('/stocks', [StockController::class, 'index'])->name('stocks');

    // 瞬間巨量
    Route::get('/five_minutes_stocks', [FiveMinutesStockController::class, 'index'])->name('five_minutes_stocks');

    // 成交倍增
    Route::get('/compare_daily_stocks', [CompareDailyStockController::class, 'index'])->name('compare_daily_stocks');

    // 模擬買賣
    Route::get('/simulation_stocks', [SimulationStockController::class, 'index'])->name('simulation_stocks');

    // 觀注股票
    Route::get('/attention_stocks', [AttentionStockController::class, 'index'])->name('attention_stocks');

    // 連續上漲
    Route::get('/rise_up_stocks', [RiseUpController::class, 'index'])->name('rise_up_stocks');

    // 歷年股利
    Route::get('/dividend', [DividendController::class, 'index'])->name('dividend');

    // 設定
    Route::get('/setting', [SettingController::class, 'index'])->name('setting');

    // 欲買股票
    Route::get('/like_stocks', [LikeStockController::class, 'index'])->name('like_stocks');
});
