@setup
    $dir = "/var/www/taiwan-stock";
@endsetup

@servers(['localhost' => '127.0.0.1'])

@story('deploy', ['on' => 'localhost'])
    git
    composer
    artisan
@endstory

@task('git')
    cd {{ $dir }}
    git checkout .
    git checkout {{ $branch }}
    git pull origin {{ $branch }}
@endtask

@task('composer')
    cd {{ $dir }}
    composer install --no-dev --no-interaction
@endtask

@task('artisan')
    cd {{ $dir }}
    php artisan migrate --force
    php artisan cache:clear
    php artisan config:cache
    php artisan route:cache
    php artisan queue:restart
@endtask
