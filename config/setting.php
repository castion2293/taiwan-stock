<?php

return [
    // slack webhook 通知相關
    // 命名規則 根據 Notification 名稱 ex: StocksNotification => stocks_notification_enable
    'slack_webhook' => [
        'stocks_notification_enable' => true, // 瞬間激增告警開關
        'compare_stock_notification_enable' => true, // 每日激增告警開關
        'attention_stock_notification_enable' => true, // 關注股票告警開關
        'golden_cross_stock_notification_enable' => true, // 均線黃金交叉股票告警開關
        'turn_up_stock_notification_enable' => true, // 均線反轉上揚股票告警開關
    ],
];
