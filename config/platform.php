<?php

return [
    // 鉅亨網股市URL
    'taiwan_stock_url' => env('TAIWAN_STOCK_URL', ''),
    // 鉅亨網股市 股利 API URL
    'dividend_api_url' => env('DIVIDEND_API_URL', ''),
    // 鉅亨網股市 均線黃金交叉 URL
    'golden_cross_url' => env('GOLDEN_CROSS_URL', ''),
    // 鉅亨網股市 均線死亡交叉 URL
    'death_cross_url' => env('DEATH_CROSS_URL', ''),
    // 鉅亨網股市 均線方向改變 URL
    'direction_turn_url' => env('DIRECTION_TURN_URL', ''),

    # slack 開關
    'slack_enable' => filter_var(env('SLACK_ENABLE', 'no'), FILTER_VALIDATE_BOOLEAN),
    // slack 推播通知 ULR
    'slack_webhook_url' => env('SLACK_WEBHOOK_URL', ''),
];
