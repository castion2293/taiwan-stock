<?php

return [
    'slack_webhook' => 'Slack 告警通知',
    'stocks_notification_enable' => '瞬間激增告警開關',
    'compare_stock_notification_enable' => '每日激增告警開關',
    'attention_stock_notification_enable' => '關注股票告警開關',
    'golden_cross_stock_notification_enable' => '均線黃金交叉股票告警開關',
    'turn_up_stock_notification_enable' => '均線反轉上揚股票告警開關',
];
