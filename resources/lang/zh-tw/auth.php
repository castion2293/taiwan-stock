<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '憑證不符合紀錄',
    'password' => '密碼錯誤',
    'throttle' => '嘗試登入太多次. 請等待 :seconds 秒後再試',

    // 註冊頁面
    'register' => [
        'name' => '姓名',
        'email' => '電子郵件',
        'password' => '密碼',
        'confirm_password' => '再次驗證密碼',
        'already_registered' => '已經註冊過?',
        'register' => '註冊'
    ],

    // 登入頁面
    'login' => [
        'email' => '電子郵件',
        'password' => '密碼',
        'remember_me' => '記住密碼',
        'forgot_password' => '忘記密碼?',
        'login' => '登入'
    ],

    // 上方列
    'navigation_dropdown' => [
        'manage_account' => '帳號管理',
        'profile' => '個人帳號',
        'logout' => '登出',
    ],

    // 個人帳號
    'profile' => [
        'profile' => '個人帳號',
        'profile_information' => '個人資料',
        'profile_information_describe' => '修改個人電子郵件及名稱',
        'name' => '姓名',
        'email' => '電子郵件',
        'save' => '存擋',
        'saved' => '已存擋',
        'update_password' => '修改密碼',
        'update_password_describe' => '確保密碼使用長度足夠及任意碼',
        'current_password' => '目前密碼',
        'new_password' => '新密碼',
        'confirm_password' => '再次驗證密碼',
        'delete_account' => '刪除帳號',
        'permanently_delete_account_describe' => '永久刪除帳號',
        'delete_account_describe' => '當帳號被刪除後，所有資料將會遺失，請確保資料已經備份',
        'delete' => '刪除',
        'delete_account_describe2' => '確定要刪除帳號嗎? 當帳號被刪除後，所有資料將會遺失，請輸入密碼來刪除帳號',
        'never_mind' => '忽略',
    ],
];
