<?php

return [
    /**
     * HTTP STATUS CODE : ２００
     */

    // 執行成功
    'success'              => 200001,

    /**
     * HTTP STATUS CODE    : ４０４
     */

    // 查無資料
    'notFound'             => 404001,

    /**
     * HTTP STATUS CODE    : ５００
     */

    // 未傳遞 API Code
    'notAPICode'           => 500001,
];
