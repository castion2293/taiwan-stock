<?php

return [
    // simulation_stocks table status
    'simulation_stocks' => [
        'buy' => 1, // 買入
        'sell' => 2, // 賣出
    ],

    // attention_stocks table status
    'attention_stocks' => [
        'attending' => 1, // 關注中
        'attended' => 2, // 已達標
        'not_attend' => 3, // 停止關注

        'greater' => 1, // 大於目標價
        'smaller' => 0, // 小於目標價
    ],

    // like_stocks table status
    'like_stocks' => [
        'add_like' => 1, // 加入
        'remove_like' => 2, // 移除
    ],

    // 執行動作 action
    'action' => [
        'buy_stock' => 'buy_simulation_stock',
        'bought' => 'bought',
        'sell_stock' => 'sell_simulation_stock',
        'add_attention' => 'add_attention_stock',
        'stop_attention' => 'stop_attention_stock',
        'like_stock' => 'like_stock',
        'remove_like_stock' => 'remove_like_stock',
        'removed_like_stock' => 'removed_like_stock',
        'reset_simulation_stock' => 'reset_simulation_stock',
        'reset_like_stock' => 'reset_like_stock',
    ]
];
