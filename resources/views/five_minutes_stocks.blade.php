<x-app-layout>

    <div class="py-3">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @livewire('filter', [
                'url' => request()->url(),
                'params' => request()->all(),
                'models' => [
                    [
                        'modelCode' => 'stockNo',
                        'modelLabel' => '股票代號 / 股票名稱',
                        'modelActive' => true
                    ],
                    [
                        'modelCode' => 'date',
                        'modelLabel' => '交易日',
                        'modelActive' => true
                    ]
                ]
            ])
        </div>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                @livewire('five-minutes-stock', [
                    'viewData' => $viewData,
                    'url' => request()->url(),
                    'params' => request()->all()
                ])
                @livewire('pagination', ['viewData' => $viewData])
            </div>
        </div>
    </div>
</x-app-layout>

