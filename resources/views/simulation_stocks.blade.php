<x-app-layout>
    <div class="py-3">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @livewire('filter', [
                'url' => request()->url(),
                'params' => request()->all(),
                'models' => [
                    [
                        'modelCode' => 'stockNo',
                        'modelLabel' => '股票代號 / 股票名稱',
                        'modelActive' => true
                    ],
                    [
                        'modelCode' => 'buy_date',
                        'modelLabel' => '購買日期',
                        'modelActive' => request()->input('status') === (string)platform('status_code.simulation_stocks.buy')
                    ],
                    [
                        'modelCode' => 'sale_date',
                        'modelLabel' => '販賣日期',
                        'modelActive' => request()->input('status') === (string)platform('status_code.simulation_stocks.sell')
                    ],
                ]
            ])
        </div>
    </div>

    <div class="py-3">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

                {{-- 上方 Tab--}}
                <div class="flex justify-between h-16">
                    @livewire('tab', [
                        'url' => request()->url(),
                        'params' => request()->all(),
                        'navs' => [
                            [
                                'name' => '庫存損益',
                                'active' => (request()->input('status') === (string)platform('status_code.simulation_stocks.buy')),
                                'code' => (string)platform('status_code.simulation_stocks.buy'),
                                'keep_filter' => [
                                    'stock_no' , 'buy_date'
                                ]
                            ],
                            [
                                'name' => '已實損益',
                                'active' => (request()->input('status') === (string)platform('status_code.simulation_stocks.sell')),
                                'code' => (string)platform('status_code.simulation_stocks.sell'),
                                'keep_filter' => [
                                    'stock_no' , 'buy_date'
                                ]
                            ],
                        ]
                    ])
                </div>

                @livewire('simulation-stocks', [
                    'viewData' => $viewData,
                    'url' => request()->url(),
                    'params' => request()->all()
                ])
                @livewire('pagination', ['viewData' => $viewData])
            </div>
        </div>
    </div>
</x-app-layout>

