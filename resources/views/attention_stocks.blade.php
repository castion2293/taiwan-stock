<x-app-layout>
    <div class="py-3">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @livewire('filter', [
                'url' => request()->url(),
                'params' => request()->all(),
                'models' => [
                    [
                        'modelCode' => 'stockNo',
                        'modelLabel' => '股票代號 / 股票名稱',
                        'modelActive' => true
                    ]
                ]
            ])
        </div>
    </div>

    <div class="py-3">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

                {{-- 上方 Tab--}}
                <div class="flex justify-between h-16">
                    @livewire('tab', [
                        'url' => request()->url(),
                        'params' => request()->all(),
                        'navs' => [
                            [
                                'name' => '觀注中',
                                'active' => (request()->input('status') === (string)platform('status_code.attention_stocks.attending')),
                                'code' => (string)platform('status_code.attention_stocks.attending'),
                                'keep_filter' => ['stock_no']
                            ],
                            [
                                'name' => '已達標',
                                'active' => (request()->input('status') === (string)platform('status_code.attention_stocks.attended')),
                                'code' => (string)platform('status_code.attention_stocks.attended'),
                                'keep_filter' => ['stock_no']
                            ],
                            [
                                'name' => '停止觀注',
                                'active' => (request()->input('status') === (string)platform('status_code.attention_stocks.not_attend')),
                                'code' => (string)platform('status_code.attention_stocks.not_attend'),
                                'keep_filter' => ['stock_no']
                            ],
                        ]
                    ])

                    @if(request()->input('status') === '1')
                        <div class="hidden sm:flex sm:items-center sm:ml-6">
                            @livewire('action-button', [
                                'data' => [
                                    'action' => platform('status_code.action.add_attention'),
                                    'greater' => (string)platform('status_code.attention_stocks.greater'),
                                ],
                                'action' => platform('status_code.action.add_attention'),
                                'url' => request()->url(),
                                'params' => request()->all()
                            ])
                            <div class="ml-6"></div>
                        </div>
                    @endif
                </div>

                @livewire('attention-stocks', [
                    'viewData' => $viewData,
                    'url' => request()->url(),
                    'params' => request()->all()
                ])
                @livewire('pagination', ['viewData' => $viewData])
            </div>
        </div>
    </div>
</x-app-layout>

