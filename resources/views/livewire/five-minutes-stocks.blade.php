@inject("fiveMinutesStockPresenter", "App\Presenters\FiveMinutesStockPresenter")
<div class="flex flex-col">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                    <tr>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            股票代號
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            股票名稱
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            交易日
                        </th>
                        <th scope="col"
                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            style="cursor: pointer"
                            wire:click="sort('new_five_minutes_stocks_sort')"
                        >
                            5分鐘內成交爆增量 {!! $fiveMinutesStockPresenter->sortSign(request()->input('new_five_minutes_stocks_sort')) !!}
                        </th>
                        <th scope="col"
                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            style="cursor: pointer"
                            wire:click="sort('stocks_sort')"
                        >
                            現在成交量 {!! $fiveMinutesStockPresenter->sortSign(request()->input('stocks_sort')) !!}
                        </th>
                        <th scope="col"
                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                            爆量時價格
                        </th>
                        <th scope="col"
                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            style="cursor: pointer"
                            wire:click="sort('price_diff_sort')"
                        >
                            漲跌價差 {!! $fiveMinutesStockPresenter->sortSign(request()->input('price_diff_sort')) !!}
                        </th>
                        <th scope="col"
                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            style="cursor: pointer"
                            wire:click="sort('price_diff_percent_sort')"
                        >
                            價差百分比 {!! $fiveMinutesStockPresenter->sortSign(request()->input('price_diff_percent_sort')) !!}
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            現在價格
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            最新更新時間
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                            <span class="sr-only">Edit</span>
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                            <span class="sr-only">Edit</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        @foreach($viewData['data'] as $data)
                            <tr>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">
                                        {{ $data['stock_no'] }}
                                        @if ($data['bought'])
                                            <i class="fa fa-credit-card text-gray-500" aria-hidden="true"></i>
                                        @endif
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['stock_name'] }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['date'] }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $fiveMinutesStockPresenter->numberFormat($data['new_five_minutes_stocks']) }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $fiveMinutesStockPresenter->numberFormat($data['stocks']) }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['five_minutes_price'] }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-{{ $fiveMinutesStockPresenter->color($data['price_diff']) }}">{{ $data['price_diff'] }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-{{ $fiveMinutesStockPresenter->color($data['price_diff']) }}">{{ $data['price_diff_percent'] }}%</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['end_price'] }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['renew_at'] }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                    @livewire('action-button', [
                                        'data' => $data,
                                        'action' => data_get($data, 'like'),
                                        'url' => $url,
                                        'params' => $params
                                    ])
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                    @livewire('action-button', [
                                        'data' => $data,
                                        'action' => platform('status_code.action.buy_stock'),
                                        'url' => $url,
                                        'params' => $params
                                    ])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
