<x-jet-dialog-modal wire:model="open">
    <x-slot name="title">
        @if ($action === platform('status_code.action.add_attention'))
            新增觀注股票
        @else
            {{ data_get($viewData, 'stock_name') }}({{ data_get($viewData, 'stock_no') }})
        @endif
    </x-slot>

    <x-slot name="content">
        @switch ($action)
            @case (platform('status_code.action.buy_stock'))
                <div class="flex-1 ml-6">
                    <label for="target_price" class="col-span-2 block text-sm font-medium text-gray-700">購買張數</label>
                    <input id="target_price" class="form-input rounded-md shadow-sm" wire:model.lazy="buyStocks">
                </div>
                @break

            @case (platform('status_code.action.sell_stock'))
                <div class="flex-1 ml-6">
                    <label for="target_price" class="col-span-2 block text-sm font-medium text-gray-700">販賣張數</label>
                    <input id="target_price" class="form-input rounded-md shadow-sm" wire:model.lazy="sellStocks">
                </div>
                @break

            @case (platform('status_code.action.add_attention'))
                <div class="flex flex-lg-nowrap">
                    <div class="flex-1 ml-6">
                        <label for="stock_no" class="col-span-2 block text-sm font-medium text-gray-700">股票代號</label>
                        <input id="stock_no" class="form-input rounded-md shadow-sm" wire:model.lazy="viewData.stock_no" wire:change="getCurrentPrice">
                        <div>
                            <span {{ $hidden ? 'hidden' : ''  }}>目前價格: {{ $currentPrice }}</span>
                        </div>
                    </div>
                    <div class="flex-1 ml-6">
                        <div class="mt-5"></div>
                        <select class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                wire:model="viewData.greater"
                        >
                            <option value="{{ platform('status_code.attention_stocks.greater') }}">高於</option>
                            <option value="{{ platform('status_code.attention_stocks.smaller') }}">低於</option>
                        </select>
                    </div>
                    <div class="flex-1 ml-6">
                        <label for="target_price" class="col-span-2 block text-sm font-medium text-gray-700">目標價</label>
                        <input id="target_price" class="form-input rounded-md shadow-sm" wire:model.lazy="viewData.target_price">
                    </div>
                </div>
                @break

            @case (platform('status_code.action.stop_attention'))
                確定停止觀注
                @break

            @case (platform('status_code.action.like_stock'))
                確定加入欲買清單
                @break

            @case (platform('status_code.action.remove_like_stock'))
                確定從欲買清單移除
                @break

            @case (platform('status_code.action.reset_simulation_stock'))
                確定重置該模擬買入股票均線狀態
                @break

            @case (platform('status_code.action.reset_like_stock'))
                確定重置該欲買股票均線狀態
                @break

            @default
                @break

        @endswitch
    </x-slot>

    <x-slot name="footer">
        <x-jet-secondary-button wire:click="$toggle('open')" wire:loading.attr="disabled">
            取消
        </x-jet-secondary-button>

        <x-jet-danger-button class="ml-2" wire:click="doAction" wire:loading.attr="disabled">
            確定
        </x-jet-danger-button>
    </x-slot>
</x-jet-dialog-modal>
