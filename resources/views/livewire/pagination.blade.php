<div class="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
    <div class="sm:flex-1 sm:flex sm:items-center sm:justify-between">
        <div>
            <p class="text-sm text-gray-700">
                <span class="font-medium">第{{ $viewData['current_page'] }}頁</span>
                <span class="font-medium m-6">共{{ $viewData['last_page'] }}頁</span>
                <span class="font-medium m-6">總共{{ $viewData['total'] }}筆</span>
            </p>
        </div>
        <div class="ml-6">
            <nav class="relative z-0 inline-flex shadow-sm -space-x-px" aria-label="Pagination">
                <a href="{{ $firstPath }}" class="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                    <i class="fa fa-angle-double-left fa-1x" aria-hidden="true"></i>
                </a>
                <a href="{{ $prevPath }}" class="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                    <i class="fa fa-chevron-left fa-1x" aria-hidden="true"></i>
                </a>
                <a href="{{ $nextPath }}" class="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                    <i class="fa fa-chevron-right fa-1x" aria-hidden="true"></i>
                </a>
                <a href="{{ $lastPath }}" class="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                    <i class="fa fa-angle-double-right fa-1x" aria-hidden="true"></i>
                </a>
            </nav>
        </div>
    </div>
</div>
