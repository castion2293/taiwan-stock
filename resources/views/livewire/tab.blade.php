<div class="flex">
    @foreach($navs as $nav)
        <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
            <x-jet-nav-link
                href="{{ $nav['url'] }}"
                :active="$nav['active']"
            >
                {{ $nav['name'] }}
            </x-jet-nav-link>
        </div>
    @endforeach
</div>
