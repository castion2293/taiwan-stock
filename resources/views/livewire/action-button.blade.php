@switch ($action)
    @case (platform('status_code.action.buy_stock'))
        <a href="#" wire:click.prevent="popModal" class="text-indigo-600 hover:text-indigo-900">買入</a>
        @break

    @case (platform('status_code.action.sell_stock'))
        <a href="#" wire:click.prevent="popModal" class="text-indigo-600 hover:text-indigo-900">賣出</a>
        @break

    @case (platform('status_code.action.add_attention'))
        <button wire:click.prevent="popModal" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
            <i class="fa fa-plus fa-1x" aria-hidden="true"></i>
        </button>
        @break

    @case (platform('status_code.action.stop_attention'))
        <a href="#" wire:click.prevent="popModal" class="text-indigo-600 hover:text-indigo-900">停止觀注</a>
        @break

    @case (platform('status_code.action.like_stock'))
        <a href="#" wire:click.prevent="popModal" class="text-indigo-600 hover:text-indigo-900">欲買</a>
        @break

    @case (platform('status_code.action.remove_like_stock'))
        <a href="#" wire:click.prevent="popModal" class="text-indigo-600 hover:text-indigo-900">移除</a>
        @break

    @case (platform('status_code.action.removed_like_stock'))
        <div class="text-sm text-gray-900">已加</div>
        @break

    @case (platform('status_code.action.reset_simulation_stock'))
        <a href="#" wire:click.prevent="popModal" class="text-indigo-600 hover:text-indigo-900">重置</a>
        @break

    @case (platform('status_code.action.reset_like_stock'))
        <a href="#" wire:click.prevent="popModal" class="text-indigo-600 hover:text-indigo-900">重置</a>
        @break

    @default
        <div class="text-sm text-gray-900">已買</div>
        @break

@endswitch


