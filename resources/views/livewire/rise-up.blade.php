@inject("riseUpStockPresenter", "App\Presenters\RiseUpStockPresenter")
<div class="flex flex-col">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                    <tr>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            股票代號
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            股票名稱
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            最後結算交易日
                        </th>
                        <th scope="col"
                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            style="cursor: pointer"
                            wire:click="sort('recent_rise_up_days_sort')"
                        >
                            最近連續上漲 {!! $riseUpStockPresenter->sortSign(request()->input('recent_rise_up_days_sort')) !!}
                        </th>
                        <th scope="col"
                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            style="cursor: pointer"
                            wire:click="sort('recent_red_k_days_sort')"
                        >
                            最近連續紅K {!! $riseUpStockPresenter->sortSign(request()->input('recent_red_k_days_sort')) !!}
                        </th>
                        <th scope="col"
                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            style="cursor: pointer"
                            wire:click="sort('longest_rise_up_days_sort')"
                        >
                            最長連續上漲 {!! $riseUpStockPresenter->sortSign(request()->input('longest_rise_up_days_sort')) !!}
                        </th>
                        <th scope="col"
                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            style="cursor: pointer"
                            wire:click="sort('longest_red_k_days_sort')"
                        >
                            最長連續紅K {!! $riseUpStockPresenter->sortSign(request()->input('longest_red_k_days_sort')) !!}
                        </th>
{{--                        <th scope="col"--}}
{{--                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"--}}
{{--                            style="cursor: pointer"--}}
{{--                            wire:click="sort('monthly_rise_up_days_sort')"--}}
{{--                        >--}}
{{--                            整月上漲天數 {!! $riseUpStockPresenter->sortSign(request()->input('monthly_rise_up_days_sort')) !!}--}}
{{--                        </th>--}}
{{--                        <th scope="col"--}}
{{--                            class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"--}}
{{--                            style="cursor: pointer"--}}
{{--                            wire:click="sort('monthly_red_k_days_sort')"--}}
{{--                        >--}}
{{--                            整月紅K天數 {!! $riseUpStockPresenter->sortSign(request()->input('monthly_red_k_days_sort')) !!}--}}
{{--                        </th>--}}
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            現在價格
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                            <span class="sr-only">Edit</span>
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                            <span class="sr-only">Edit</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        @foreach($viewData['data'] as $data)
                            <tr>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">
                                        {{ $data['stock_no'] }}
                                        @if ($data['bought'])
                                            <i class="fa fa-credit-card text-gray-500" aria-hidden="true"></i>
                                        @endif
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">
                                        {{ $data['stock_name'] }}
                                        @if ($data['gold_cross'])
                                            <i class="fa fa-arrows sm-1 text-red-500" aria-hidden="true"></i>
                                        @endif
                                        @if ($data['turn_up'])
                                            <i class="fa fa-level-up sm-1 text-red-500" aria-hidden="true"></i>
                                        @endif
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['end_date'] }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['recent_rise_up_days'] }}天</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['recent_red_k_days'] }}天</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['longest_rise_up_days'] }}天</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['longest_red_k_days'] }}天</div>
                                </td>
{{--                                <td class="px-6 py-4 whitespace-nowrap">--}}
{{--                                    <div class="text-sm text-gray-900">{{ $data['monthly_rise_up_days'] }}天</div>--}}
{{--                                </td>--}}
{{--                                <td class="px-6 py-4 whitespace-nowrap">--}}
{{--                                    <div class="text-sm text-gray-900">{{ $data['monthly_red_k_days'] }}天</div>--}}
{{--                                </td>--}}
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $data['end_price'] }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                    @if (data_get($data, 'action') !== platform('status_code.action.bought'))
                                        @livewire('action-button', [
                                            'data' => $data,
                                            'action' => data_get($data, 'like'),
                                            'url' => $url,
                                            'params' => $params
                                        ])
                                    @endif
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                    @livewire('action-button', [
                                        'data' => $data,
                                        'action' => platform('status_code.action.buy_stock'),
                                        'url' => $url,
                                        'params' => $params
                                    ])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
