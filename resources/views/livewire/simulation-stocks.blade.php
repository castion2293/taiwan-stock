@inject("simulationStockPresenter", "App\Presenters\SimulationStockPresenter")
<div class="flex flex-col">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                @if (Arr::get($params, 'status') === (string)platform('status_code.simulation_stocks.buy'))
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead>
                            <tr>
                                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    股票代號
                                </th>
                                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    股票名稱
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('buy_stocks_sort')"
                                >
                                    購買張數 {!! $simulationStockPresenter->sortSign(request()->input('buy_stocks_sort')) !!}
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                    成交均價
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                    付出成本
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('buy_date_sort')"
                                >
                                    購買日期 {!! $simulationStockPresenter->sortSign(request()->input('buy_date_sort')) !!}
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('end_price_sort')"
                                >
                                    現在價格 {!! $simulationStockPresenter->sortSign(request()->input('end_price_sort')) !!}
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('price_diff_sort')"
                                >
                                    預估損益 {!! $simulationStockPresenter->sortSign(request()->input('price_diff_sort')) !!}
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('price_diff_percent_sort')"
                                >
                                    報酬率 {!! $simulationStockPresenter->sortSign(request()->input('price_diff_percent_sort')) !!}
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('today_price_diff_sort')"
                                >
                                    今日漲跌 {!! $simulationStockPresenter->sortSign(request()->input('today_price_diff_sort')) !!}
                                </th>
                                <th scope="col" class="px-6 py-3 bg-gray-50">
                                    <span class="sr-only">Edit</span>
                                </th>
                                <th scope="col" class="px-6 py-3 bg-gray-50">
                                    <span class="sr-only">Edit</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                            @foreach($viewData['data'] as $data)
                                <tr>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ data_get($data, 'stock_no') }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">
                                            {{ data_get($data, 'stock_name') }}
                                            @if ($data['death_cross'])
                                                <i class="fa fa-arrows sm-1 text-green-500" aria-hidden="true"></i>
                                            @endif
                                            @if ($data['turn_down'])
                                                <i class="fa fa-level-down sm-1 text-green-500" aria-hidden="true"></i>
                                            @endif
                                            @if ($data['gold_cross'])
                                                <i class="fa fa-arrows sm-1 text-red-500" aria-hidden="true"></i>
                                            @endif
                                            @if ($data['turn_up'])
                                                <i class="fa fa-level-up sm-1 text-red-500" aria-hidden="true"></i>
                                            @endif
                                            @if ($data['compare_daily_stock'])
                                                <i class="fa fa-free-code-camp sm-1 text-red-500" aria-hidden="true"></i>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ $simulationStockPresenter->numberFormat(data_get($data, 'buy_stocks')) }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ data_get($data, 'buy_price') }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ $simulationStockPresenter->numberFormat(data_get($data, 'cost')) }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ data_get($data, 'buy_date') }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ data_get($data, 'end_price') }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-{{ $simulationStockPresenter->color(data_get($data, 'price_diff')) }}">{{ $simulationStockPresenter->numberFormat(data_get($data, 'price_diff')) }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-{{ $simulationStockPresenter->color(data_get($data, 'price_diff_percent')) }}">
                                            {{ $simulationStockPresenter->numberFormat(data_get($data, 'price_diff_percent'), 2) }}%
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-{{ $simulationStockPresenter->color(data_get($data, 'today_price_diff')) }}">{{ data_get($data, 'today_price_diff') }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                        @if ($data['death_cross'] || $data['turn_down'] || $data['gold_cross'] || $data['turn_up'] || $data['compare_daily_stock'])
                                            @livewire('action-button', [
                                                'data' => $data,
                                                'action' => platform('status_code.action.reset_simulation_stock'),
                                                'url' => $url,
                                                'params' => $params
                                            ])
                                        @endif
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                        @livewire('action-button', [
                                            'data' => $data,
                                            'action' => data_get($data, 'action'),
                                            'url' => $url,
                                            'params' => $params
                                        ])
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
                @else
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead>
                            <tr>
                                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    股票代號
                                </th>
                                <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    股票名稱
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('sale_stocks_sort')"
                                >
                                    販賣張數 {!! $simulationStockPresenter->sortSign(request()->input('sale_stocks_sort')) !!}
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('buy_price_sort')"
                                >
                                    購買成交均價 {!! $simulationStockPresenter->sortSign(request()->input('buy_price_sort')) !!}
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('sale_price_sort')"
                                >
                                    販賣價格 {!! $simulationStockPresenter->sortSign(request()->input('sale_price_sort')) !!}
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('sale_date_sort')"
                                >
                                    販賣日期 {!! $simulationStockPresenter->sortSign(request()->input('sale_date_sort')) !!}
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('profit_loss_sort')"
                                >
                                    損益 {!! $simulationStockPresenter->sortSign(request()->input('profit_loss_sort')) !!}
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                    style="cursor: pointer"
                                    wire:click="sort('profit_loss_percent_sort')"
                                >
                                    報酬率 {!! $simulationStockPresenter->sortSign(request()->input('profit_loss_percent_sort')) !!}
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                            @foreach($viewData['data'] as $data)
                                <tr>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ $data['stock_no'] }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ $data['stock_name'] }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ $simulationStockPresenter->numberFormat($data['sale_stocks']) }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ $data['buy_price'] }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ $data['sale_price'] }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ $data['sale_date'] }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-{{ $simulationStockPresenter->color($data['profit_loss']) }}">{{ $simulationStockPresenter->numberFormat($data['profit_loss']) }}</div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-{{ $simulationStockPresenter->color($data['profit_loss_percent']) }}">{{ $data['profit_loss_percent'] }}%</div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
