@foreach ($viewData as $key => $data)
<div class="grid grid-cols-6 gap-4">
    <div class="">
        <div class="col-span-1">
            {{ __('setting.slack_webhook') }}
        </div>
    </div>
    <div class=""></div>
    <div class="">
        @foreach ($data as $valueKey => $value)
            <div class="col-span-1 sm:col-span-4">
                <div class="flex items-center mt-5">
                    <label for="{{ $valueKey }}" class="flex items-center cursor-pointer bg-gray-100">
                        <!-- toggle -->
                        <div class="relative">
                            <!-- input -->
                            <input id="{{ $valueKey }}" type="checkbox" class="hidden" wire:model.lazy="viewData.{{ $key }}.{{ $valueKey }}">
                            <!-- line -->
                            <div class="toggle__line w-10 h-4 bg-white rounded-full shadow-inner"></div>
                            <!-- dot -->
                            <div class="toggle__dot absolute w-6 h-6 bg-gray-500 rounded-full shadow inset-y-0 left-0"></div>
                        </div>
                    </label>
                    <div class="flex items-center text-gray-700 font-medium absolute ml-12">{{ __("setting.{$valueKey}") }}</div>
                </div>
            </div>
        @endforeach
    </div>
    <div class=""></div>
    <div class=""></div>
    <div class="">
        <div class="mt-12">
            <button wire:click="updateSetting('{{ $key }}')" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                確認
            </button>
        </div>
        @if (session()->has('message'))
            <div class="alert alert-success text-red-500">
                {{ session('message') }}
            </div>
        @endif
    </div>
</div>
@endforeach

<x-jet-section-border/>

<style>
    .toggle__dot {
        top: -.25rem;
        left: -.25rem;
        transition: all 0.3s ease-in-out;
    }

    input:checked ~ .toggle__dot {
        transform: translateX(100%);
        background-color: #48bb78;
    }
</style>
