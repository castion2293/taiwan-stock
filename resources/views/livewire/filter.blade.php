<div class="shadow overflow-hidden sm:rounded-md">
    <div class="px-4 py-5 bg-white sm:p-6">
        <div class="flex">
            @foreach ($filterParams as $code => $filterParam)
                <div class="flex-1 ml-6">
                    <label for="stock_no" class="col-span-2 block text-sm font-medium text-gray-700">{{ $filterParam['label'] }}</label>
                    <input id="stock_no" class="form-input rounded-md shadow-sm" wire:model.lazy="filterParams.{{ $code }}.model">
                </div>
            @endforeach
        </div>
    </div>
    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
        <button wire:click="resetUrl" class="inline-flex items-center justify-center px-4 py-2 bg-red-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red active:bg-red-600 transition ease-in-out duration-150">
            重設
        </button>

        <button wire:click="submitUrl" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
            確認
        </button>
    </div>
</div>
